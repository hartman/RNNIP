#!/bin/bash -u

# Compare training with the hits vs category embedding
#bsub -W 80:00 -q slacgpu -o "output/hits_3m.txt"  "python trainNet.py"
# bsub -W 80:00 -q slacgpu -o "output/hits_5m.txt"  "python trainNet.py --nJets 5000000"
# bsub -W 80:00 -q slacgpu -o "output/grade_5m.txt" "python trainNet.py --doEmbedding --nJets 5000000"

# To run on the docker slac gpu container, need to be able to just run directly
#bsub -W 80:00 -q slacgpu -o "output/hits_200k.txt"  "python trainNet.py --doEmbedding --nJets 200000" 
#bsub -W 80:00 -q slacgpu -o "output/hits_500k.txt" "python trainNet.py --nJets 500000" 
#bsub -W 80:00 -q slacgpu -o "output/hits_1m.txt" "python trainNet.py --nJets 1000000" 

# Try some permutations for the inputs to the net
# bsub -W 80:00 -q slacgpu -o "output/noBLHits_3m.txt"  "python trainNet.py"
# bsub -W 80:00 -q slacgpu -o "output/holes_3m.txt"  "python trainNet.py --jointNormVars 'nNextToInnHits,nInnHits,nsharedBLHits,nsplitBLHits,nPixHits,nPixHoles,nsharedPixHits,nsplitPixHits,nSCTHits,nSCTHoles,nsharedSCTHits'"


# Unwhitened: default cat
# bsub -W 80:00 -q slacgpu -o "output/unwhitened_defaultCat_3m.txt"  "python trainNet.py --noNormVars sd0,sz0,nNextToInnHits,nInnHits,nsharedBLHits,nsplitBLHits,nPixHits,nsharedPixHits,nsplitPixHits,nSCTHits,nsharedSCTHits --jointNormVars -1"
# 
# # Unwhitened: with SCTHoles
# bsub -W 80:00 -q slacgpu -o "output/unwhitened_SCTHoles_3m.txt"  "python trainNet.py --noNormVars sd0,sz0,nNextToInnHits,nInnHits,nsharedBLHits,nsplitBLHits,nPixHits,nsharedPixHits,nsplitPixHits,nSCTHits,nSCTHoles,nsharedSCTHits --jointNormVars -1"

# # Whitened: default cat
# bsub -W 80:00 -q slacgpu -o "output/defaultCat_3m.txt"  "python trainNet.py"
# 
# # Whitened: with SCTHoles
# bsub -W 80:00 -q slacgpu -o "output/SCTHoles_3m.txt"  "python trainNet.py --jointNormVars nNextToInnHits,nInnHits,nsharedBLHits,nsplitBLHits,nPixHits,nsharedPixHits,nsplitPixHits,nSCTHits,nSCTHoles,nsharedSCTHits"

# Models with the auxiliary loss function
#bsub -W 80:00 -q atlas-t3 -o "output/trk_5class.txt"  "python updatedTrainNet.py --model LSTM_trkClass"
bsub -W 80:00 -q atlas-t3 -o "output/trk_4class_alpha1.txt"  "python trainNet.py --model LSTM_trkClass --nTrkClasses 4 --alpha 1"
bsub -W 80:00 -q atlas-t3 -o "output/trk_4class_alpha2.txt"  "python trainNet.py --model LSTM_trkClass --nTrkClasses 4 --alpha 2"
bsub -W 80:00 -q atlas-t3 -o "output/trk_4class_alpha1pt5.txt"  "python trainNet.py --model LSTM_trkClass --nTrkClasses 4 --alpha 1.5"
bsub -W 80:00 -q atlas-t3 -o "output/trk_4class_alpha0pt5.txt"  "python trainNet.py --model LSTM_trkClass --nTrkClasses 4 --alpha 0.5"





