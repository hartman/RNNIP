'''

concatFiles.py

Use the df and xr files created by the parallelPreprocessing script to concatenate them with a desired number
of events for the training and test files, which different options for the scaling files for the test sets.

'''
import os
from usefulFcts import nJetsTag, strToList

if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--trainJetCollection', type=str, default='PFlow',
                   help="Jet collection to use for the training.")
    p.add_argument('--testJetCollection', type=str, default='PFlow',
                   help="Jet collection to use for testing.")
    p.add_argument('--train_mc',type=str, default='mc16d', help="mc campaign for training.")
    p.add_argument('--test_mc', type=str, default='mc16d', help="mc campaign for testing.")
    p.add_argument('--batch', action="store_true",
                   help="Whether to run these commands interactivately or submit them to the LSF batch system.")
    p.add_argument('--verbose', action="store_true", help="Print the cmd before running it")
    p.add_argument('--test', action="store_true",
                   help="Get the test files (as opposed to the training files)")
    p.add_argument('--trainPhysicsSamples', type=str, default="hybrid_5TeV",
                   help="Comma separated list of the physics samples to use for "\
                   +"training processing, or for the scaling files to evaluate on "\
                   +"if you're running in the test mode. Default: hybrid_5TeV")
    p.add_argument("--trainNJets", type=str, default="5000000",
                   help="Comma separated list of the number jets for each of the "\
                   +"training physics samples. Must be the same length as "\
                   +"trainPhysicsSamples. Default 5e6.")
    p.add_argument('--testPhysicsSamples', type=str, default="ttbar,Zprime_1.5TeV",
                   help="Comma separated list of the physics samples to use for "\
                   +"each test file. Only used if the `test` argument is passed. "\
                   +"Default: ttbar,Zprime_1.5TeV")

    p.add_argument('--trainNTrks',type=str,default='15',help="The number of tracks used for"\
                   +"training. Can either be a single value (defualt 15) or a comma separated"\
                   +"list to use a different # of tracks for each training physics sample.")
    p.add_argument('--testNTrks',type=str,default='15',help="The number of tracks used for"\
                   +"testing. Also can either be a single value (defualt 15) or a comma separated"\
                   +"list to use a different # of tracks for each training physics sample.")
    args = p.parse_args()

    train_mc, test_mc, batch = args.train_mc, args.test_mc, args.batch
    trainJetCollection, testJetCollection = args.trainJetCollection, args.testJetCollection

    # Get the arguments for which physics samples to use
    trainPhysicsSamples = strToList(args.trainPhysicsSamples)
    trainNJets = [int(n) for n in strToList(args.trainNJets)]
    testPhysicsSamples = strToList(args.testPhysicsSamples)
    assert len(trainPhysicsSamples) == len(trainNJets)

    if "," in args.trainNTrks:
        trainNTrks = [int(n) for n in strToList(args.trainNTrks)]
    else:
        trainNTrks = [int(args.trainNTrks)] * len(trainPhysicsSamples)

    if "," in args.testNTrks:
        testNTrks = [int(n) for n in strToList(args.testNTrks)]
    else:
        testNTrks = [int(args.testNTrks)] * len(testPhysicsSamples)


    '''
    Ok, so there are some things that I'm keeping constant between the samples, so I can
    represent this with a bsae_cmd which I can append the info that I'm looping over
    '''

    nJetsMini = "*" #"*k"

    base_cmd = r"python root_to_np.py --mc {} --jetCollection {} --physicsSample {} --nJets {} --filename 'data/{}/files_{}trks_sd0_rev/jet_{}{}_{}trks_sd0_rev_*.h5' --mode {} --nTrks {}"
    bsubCmd = 'bsub -W 80:00 -q atlas-t3 -oo output/{}.txt -J {} {}'

    # Get the training files of interest
    if not args.test:
        mode = "train"
        #for physicsSample, Ntrain in zip(['ttbar','Zprime_1.5TeV','Zprime_5TeV'],[int(3e6),int(5e6),int(5e6)]):
        for physicsSample, Ntrain, nTrks in zip(trainPhysicsSamples,trainNJets, trainNTrks):

            subDir = "_".join([train_mc, trainJetCollection, physicsSample])
            cmd = base_cmd.format(train_mc,trainJetCollection,physicsSample,Ntrain,subDir,nTrks,nJetsMini,mode,nTrks,mode,nTrks)

            if args.verbose:
                print(cmd)

            if batch:
                job_name = '{}_{}'.format(subDir,mode)
                os.system(bsubCmd.format(job_name, job_name, cmd))
            else:
                os.system(cmd)


    # Get the test files of interest
    else:
        vStr = "sd0_sz0_nNextToInnHits_nInnHits_nsharedBLHits_nsplitBLHits_nsharedPixHits_nsplitPixHits_nsharedSCTHits_logNorm_ptfrac_dr_norm_nPixHits_nSCTHits"
        scalingfile = "data/{}/scale_{}train_15trks_{}.json"

        mode = "test"
        for physicsSample, Ntest,nTrks in zip(testPhysicsSamples,[int(1.5e6)]*len(testPhysicsSamples),testNTrks):
            for trainPhysicsSample, Ntrain, nTrks_tr in zip(trainPhysicsSamples,trainNJets,trainNTrks):

                subDir = "_".join([test_mc, testJetCollection, physicsSample])
                cmd = base_cmd.format(test_mc,testJetCollection,physicsSample,Ntest,subDir,nTrks,nJetsMini,mode,nTrks,mode,nTrks)

                trainSubDir = "_".join([train_mc, trainJetCollection, trainPhysicsSample])
                cmd += " --scalingfile data/{}/scale_{}train_{}trks_{}.json".format(trainSubDir,nJetsTag(Ntrain),nTrks_tr,vStr)

                if physicsSample != trainPhysicsSample or trainJetCollection != testJetCollection:
                    tag1 = "_{}".format(trainJetCollection) if trainJetCollection != testJetCollection else ""
                    tag2 = "_{}".format(trainPhysicsSample) if physicsSample != trainPhysicsSample else ""
                    scale_tag = tag1 + tag2 + "_scale"
                    cmd += " --scale_tag {}".format(scale_tag)
                else:
                    scale_tag = ""

                if args.verbose:
    	            print(cmd)

                if batch:
                    job_name = '{}_{}'.format(subDir,mode)
                    os.system(bsubCmd.format(job_name, job_name, cmd))
                else:
                    os.system(cmd)
