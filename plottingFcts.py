import numpy as np
import pandas as pd
from scipy.interpolate import pchip
import matplotlib.pyplot as plt
from matplotlib import gridspec
from matplotlib.colors import LogNorm
import h5py
import pylab as p
import os
import json


def trainingMetrics(modelName, modelDir='../models/mc16d'):

    '''
    Plot the training and val curves for a model.
    The history object is accessed using the model's name

    Inputs:
    - modelName: Allows you to save the training curves using the ! model name
    - subDir: If not an empty string, allows you to save the curves in a sub-directory
              inside figures.

    '''

    # Get the training loss / acc cuves
    g = h5py.File("{}/{}_history.hdf5".format(modelDir,modelName),"r")

    for key in g.keys():
        print(key)

    loss     = g['loss'][:]
    val_loss = g['val_loss'][:]

    epochs = np.arange(1,loss.size+1)

    plt.plot(epochs,loss,label='training')
    plt.plot(epochs,val_loss,label='validation')
    plt.xlabel('epochs',fontsize=14)
    plt.ylabel('cross-entropy loss',fontsize=14)
    plt.legend()
    figDir = modelDir.replace('models','figures')
    plt.savefig('{}/loss_{}.pdf'.format(figDir,modelName))

    if 'alpha' not in modelName:


        acc      = g['acc'][:]
        val_acc  = g['val_acc'][:]

        plt.figure()
        plt.plot(epochs,acc,label='training')
        plt.plot(epochs,val_acc,label='validation')

        plt.xlabel('epochs',fontsize=14)
        plt.ylabel('accuracy',fontsize=14)
        plt.legend()
        plt.tight_layout()

        plt.savefig('{}/acc_{}.pdf'.format(figDir,modelName))

    else:
        # Plot the jet and track loss / acc functions

        for metric, ylabel in zip(['loss', 'acc'],
                                  ['cross-entropy loss','accuracy']):

            plt.figure()

            plt.plot(epochs,g['Jet_class_'+metric][:],label='jet train',
            color='C0',linestyle='-')
            plt.plot(epochs,g['val_Jet_class_'+metric][:],label='jet val',
            color='C1',linestyle='-')

            plt.plot(epochs,g['Trk_class_'+metric][:],label='trk train',
            color='C0',linestyle='--')
            plt.plot(epochs,g['val_Trk_class_'+metric][:],label='trk val',
            color='C1',linestyle='--')

            plt.xlabel('epochs',fontsize=14)
            plt.ylabel(ylabel,fontsize=14)
            plt.legend()
            plt.savefig('{}/jetTrk_{}_{}.pdf'.format(figDir,metric,modelName))

    plt.show()

    g.close()

def calculateEff(myHist):

    '''
    Given a histogram, calculate the area starting from the top
    and integrating to the bottomself.

    #, disregarding the lowest bin
    # so that the area never goes all the way to 1.

    Input:
        myHist: heights of the bins in a histogram

    Output:
        eff: Cummulative area of the histogram

    '''
    return np.add.accumulate(myHist[::-1]) / np.sum(myHist)


def sigBkgEff(m, X_test, y_test, ix_test=None, returnDisc=False,
              figDir='../figures',subDir='mc16d',tag='',fc = 0.07,
              modelDir='',colors=['C0','C1','C2']):

    '''
        Given a model, make the histograms of the model outputs to get the ROC curves.

        Input:
            m: A class (LSTM or attnModel) which are children of the myModel class
            X_test: Model inputs of the test set
            y_test: Truth labels for the test set
            returnDisc: If True, also return the raw discriminant (for comparison between
                        different evaluations for the RNNIPFlip studies).
            tag: Extra tag to append to the figure name for additional specificity
            fc: The amount by which to weight the c-jet prob in the disc. The
                default value of 0.07 corresponds to the fraction of c-jet bkg
                in ttbar.
            modelDir: If not an empty string, these efficiencies will be saved to
                      be retrieved more easily later

        Output:
            effs: A list with 3 entries for the l, c, and b effs
            disc: b-tagging discriminant (will only be returned if returnDisc is True)
    '''

    # Evaluate the performance with the ROC curves!
    predictions = m.eval(X_test)

    # To make sure you're not discarding the b-values with high
    # discriminant values that you're good at classifying, use the
    # max from the distribution
    disc = np.log(np.divide(predictions[:,2], fc*predictions[:,1] + (1 - fc) * predictions[:,0]))

    # Sometimes we could have infinite values for the discriminant if pb is 1
    # and pl and pc are 0. In these cases, replace infinity with the largest
    # non-infinite value for the discriminant.
    posInf = (disc == np.inf)
    if posInf.sum() != 0:
        print('{} jets had a positive inf disc'.format(posInf.sum()))
        disc[posInf] = np.max(disc[~posInf])

    negInf = (disc == -np.inf)
    if negInf.sum() != 0:
        print('{} jets had a negative inf disc'.format(negInf.sum()))
        disc[negInf] = np.min(disc[~negInf])

    print('min: {:.2f}, max: {:.2f}'.format(np.min(disc),np.max(disc)))

    discMax = np.max(disc)
    discMin = np.min(disc)

    myRange=(discMin,discMax)
    nBins = 200

    effs = []
    for output, flavor, c in zip([0,1,2], ['l','c','b'], colors):

        ix = (y_test == output)

        # Plot the discriminant output
        nEntries, edges ,_ = plt.hist(disc[ix],alpha=0.5,label='{}-jets'.format(flavor),
                                      bins=nBins, range=myRange, color=c, density=True, log=True)

        # Calculate the baseline signal and bkg efficiencies
        eff = calculateEff(nEntries)
        effs.append(eff)

    plt.legend()
    plt.xlabel('$D = \ln [ p_b / (f_c p_c + (1- f_c)p_l ) ]$',fontsize=14)
    plt.ylabel('"Normalized" counts')
    plt.savefig('{}/{}/disc_{}{}.pdf'.format(figDir,subDir, m.modelName, tag))
    plt.show()

    # Save the roc curves
    if len(modelDir) > 0:

        # Open a file
        effs_file = open('{}/effs_{}{}.json'.format(modelDir, m.modelName, tag),'w')

        # Store the efficiencies
        outputDict = {'l':effs[0].tolist(),
                      'c':effs[1].tolist(),
                      'b':effs[2].tolist(),
                      'disc':disc.tolist()}
        json.dump(outputDict,effs_file)

        # Close the file
        effs_file.close()

    if returnDisc:
        return effs, disc
    else:
        return effs

def getEffs(m, X_test, y_test, ix_test=None, returnDisc=False,
            figDir='../figures',subDir='mc16d',tag='',fc = 0.07,
            modelDir=''):
    '''
    Wrapper around the sigBkgEff class that accepts all of the
    same arguments, but will try to just load in the dictionary
    for the efficiencies if it already exists. Otherwise, it
    calls sigBkgEff with requested argumentsself.

    This function also formats the output the same as sigBkgEff

    '''

    eff_file = "{}/effs_{}{}.json".format(modelDir,m.modelName,tag)
    print(eff_file)

    if os.path.exists(eff_file):

        print('Load in the previously saved efficiency file')
        with open(eff_file, 'r') as f:
            effDict = json.load(f)
        leff = np.array(effDict['l'])
        ceff = np.array(effDict['c'])
        beff = np.array(effDict['b'])
        disc = np.array(effDict['disc'])

        if returnDisc:
            return [leff, ceff, beff], disc
        else:
            return leff, ceff, beff

    else:
        return sigBkgEff(m, X_test, y_test, ix_test, returnDisc, figDir, subDir,
                         tag, fc, modelDir)


def workingPoint(discriminant, nBins, myRange, WP=0.7,verbose=True):
    '''

    Given an output for a discriminant, look at where to cut to acheive given
    working point (WP), (whose default is set to 70%)

    Inputs:
        disciminant: The variable you want to cut on for the tagger
        WP: The desired efficiency of the cut
        nBins: # of bins for binning the histogram
        myRange:

    Output:
        cut: The value you should use to cut on the discriminant to acheive the
             efficeincy closest to the desired working point

    '''

    assert WP >= 0 and WP <= 1 #Error! The WP should be between 0 and 1

    # histogramize these values
    hist, edges = np.histogram(discriminant, bins=nBins, range=myRange, density=True)

    # Get the truth efficiencies from the histogramed discriminant
    teff = calculateEff(hist)

    # Find the index of the efficeincy hist corresponding to this WP
    i_eff = min((abs(eff-WP),j) for j,eff in enumerate(teff))[1]

    if verbose:
        print('i_eff = {}, teff = {}'.format(i_eff,teff[i_eff]))

    # B/c of the way Ariel told me to cut on the ROC curves to eliminate
    # jets w/ 0 tracks, I need to subtract 1 to the idx location to get the correct cut.
    cut = edges[nBins-i_eff-1]

    if verbose:
        print( "Cut on discriminant at {}".format(cut))

    return cut

def plotPtDependence(jetPts, inpt_ys, discs, labels, WP=0.7, flat=False,
                     pt_edges=np.array([20, 50, 90, 150, 300, 1000]),
                     eff_bins = np.array([20,30,40,50,70,90,120,150,200,250,300,400,500,1000]),
                     binomialErrors=False, text='',xlabel='jet $p_T$ [GeV]',var='pT',
                     lrej_lim=None,crej_lim=None,beff_lim=None, verbose=True, log=False,
                     tag='', figDir='../figures',subDir='mc16d',colors=None):
    '''
    For a given list of discriminants, plot the b-eff, l and c-rej as a function
    of jet pt.

    For a list of discriminants, plot the b-eff, l-rej, and c-rej (comparing
    different algs) as a function of the jet pt

    Inputs:
    - jetPts: The pt for each jet in the test (or dev) set
    - y: The truth lables:
         - 0: l
         - 1: c
         - 2: b
    - discs: A list of the discriminants
    - labels: The labels for the legend of this plot
    - WP: The working point for the inclusive signal efficiency,
    - flat: boolean to indicate whether to calcuate for flat signal eff
    - pt_edges: The edges for the pt bins
    - eff_bins: Bins for calculating the flat signal efficiency
    - binomialErrors: Boolean for whether to include binomial errors in these
                      plots
    - text: A string of text to add to the upper left corner of the plot
    - xlabel: The label for the x-axis
    - var: The variable you're plotting to save the figure as (default pT)
    - lrej_lim: The limit for the y-axis of the light rejection
    - crej_lim: The limit for the y-axis of the charm rejection
    - beff_lim: The limit for the y-axis of the b efficinecy
    - verbose: If True, print more information about the discriminant cuts used
               for each pT bin.
    - log: Whether to use a log scale for the plots (default False)
    - tag: A tag to append to the model name
    - subDir: the subdirectory in figures to save the model too
    - colors: The colors to use for each of the discs in the histograms
              If None, python's default color scheme palatte will be used

    '''

    # Need to have the pts and ys be passed as the same type: either they're
    # both arrays, or both lists of arrays
    assert type(jetPts) == type(inpt_ys)

    # Check that all the discs have appropriate legends
    assert len(discs) == len(labels)

    # If you're not passing jetPts and inpt_ys in as lists, make lists
    if type(jetPts) == list:
        pts = jetPts
        ys = inpt_ys
    else:
        pts = [jetPts for li in labels]
        ys = [inpt_ys for li in labels]

    # Test that your inputs are the same length - at least for the first element
    assert pts[0].size == discs[0].size and pts[0].size == ys[0].size

    if colors is None:
        colors=["C{}".format(i) for i in range(len(discs))]

    # Get the edges for the histogram
    pt_midpts = (pt_edges[:-1] + pt_edges[1:]) / 2.
    bin_widths = (pt_edges[1:] - pt_edges[:-1]) / 2.
    Npts = pt_midpts.size

    for disc, pt, y, label, color in zip(discs, pts, ys, labels, colors):

        # Get the sig eff and bkg rejections
        effs = [ np.zeros(Npts) ] * 3

        # Get the cut for the discriminant.
        nBins=1000
        myRange=(np.min(disc),np.max(disc))
        if flat:
            cut = getFlatSigEff(pt,eff_bins,y,disc,myRange,title=label,
                                 fig_idx=4 if label=='ip3d' else 5,verbose=verbose)
        else:
            cut = workingPoint(disc[y==2], nBins, myRange, WP=WP,verbose=verbose)

        for i in range(3):
            # Note for later: I could probably do this a lot more cleanly
            # with np.digitize

            # For calculating the binomial errors, let nTest be an array of
            # the same shape as the the # of pT bins that we have
            nTest = np.zeros(Npts)

            for j, pt_min, pt_max in zip(np.arange(Npts), pt_edges[:-1], pt_edges[1:]):

                den_mask  = (pt > pt_min) & (pt < pt_max) & (y == i)
                num_mask  = den_mask & (disc > cut)
                nTest[j] = den_mask.sum()
                effs[i][j] = num_mask.sum() / nTest[j]

            # For b-jets, plot the eff: for l and c-jets, look at the rej
            plt.figure(i+1)
            if i == 2:
                yerr = eff_err(effs[i],nTest) if binomialErrors else None

                plt.errorbar(pt_midpts,effs[i],xerr=bin_widths, yerr=yerr,
                             color=color,fmt='.',label=label)
            else:
                rej = 1/effs[i]
                yerr = np.power(rej,2) * eff_err(effs[i],nTest) if binomialErrors else None
                plt.errorbar(pt_midpts, rej, xerr=bin_widths, yerr=yerr,
                             color=color,fmt='.',label=label)

    # Make the figures pretty + save them
    for i, (flav, ylim) in enumerate(zip(['l','c','b'],[lrej_lim,crej_lim,beff_lim])):

        metric = 'eff' if i==2 else 'rej'

        plt.figure(i+1)
        plt.xlabel(xlabel,fontsize=14)
        plt.ylabel('{}-{}'.format(flav,metric),fontsize=14)
        if log:
            plt.yscale('log')
            logTag="_log"
        else:
            logTag=""

        if verbose:
            print(WP)
        title = '{} {}% WP'.format('flat' if flat else 'fixed cut',int(WP*100))
        if len(text) > 0:
            assert ylim is not None # otherwise the logic in this block doesn't work
            plt.text(pt_edges[0],ylim[1],text, horizontalalignment='left', verticalalignment='bottom')
            plt.title(title, loc='right',fontsize=12)
        else:
            plt.title(title)

        plt.xlim(pt_edges[0],pt_edges[-1])
        if ylim is not None:
            print(flav,ylim)
            plt.ylim(ylim)
        plt.legend(loc='best',fontsize=12)

        if len(tag) > 0:
            plt.savefig('{}/{}/{}-{}_vs_{}{}_{}{}.pdf'.format(figDir,subDir,flav,metric,var,'_flatEff' if flat else '',tag,logTag),bbox_inches='tight')
    plt.show()

def getFlatSigEff(pts,eff_bins,y,disc,disc_range,title='',fig_idx=4,
                  xlabel='jet $p_T$ [GeV]',verbose=True):
    '''
    Get the FLAT signal efficiency.

    '''

    indices = np.digitize(pts,bins=eff_bins)

    cut70_flat = np.array([workingPoint(disc[(y==2) & (indices==i)],200,disc_range,verbose=verbose)
                       for i in np.arange(1,len(eff_bins))])

    # Sanity check: Plot the signal efficiency for the WP + with smaller bins
    # to verify that I used small enough bins!
    yi = 2

    sig_eff1 = np.zeros(cut70_flat.shape)
    sig_eff2 = np.zeros(cut70_flat.shape[0]*2)

    for i, pt_min, pt_max, cut in zip(np.arange(eff_bins.shape[0]),
                                      eff_bins[:-1],eff_bins[1:],cut70_flat):

        den_mask = (pts > pt_min) & (pts < pt_max) & (y == yi)
        num_mask = den_mask & (disc > cut)
        sig_eff1[i] = num_mask.sum() / den_mask.sum()

        pt_mid = .5 * (pt_min + pt_max)

        den1_mask = (pts > pt_min) & (pts < pt_mid) & (y == yi)
        num1_mask = den_mask & (disc > cut)
        sig_eff2[i*2] = num_mask.sum() / den_mask.sum()

        den2_mask = (pts > pt_mid) & (pts < pt_max) & (y == yi)
        num2_mask = den_mask & (disc > cut)
        sig_eff2[i*2+1] = num_mask.sum() / den_mask.sum()


    midpts1 = .5 * (eff_bins[:-1]+eff_bins[1:])
    eff_bins2 = np.sort(np.concatenate((eff_bins,midpts1),axis=0))
    midpts2 = .5 * (eff_bins2[:-1]+eff_bins2[1:])

    plt.figure(fig_idx)
    plt.scatter(midpts1,sig_eff1,label='Eff at 70% WP')
    plt.scatter(midpts2,sig_eff2,label='Eff at 70% WP: smaller bins')
    plt.plot([0,eff_bins[-1]],[.7]*2,'k--')
    plt.xlabel(xlabel)
    plt.ylabel('Signal eff')

    plt.ylim(0,1)
    plt.legend()
    plt.title(title)

    #plt.show()

    # What do I actually WANT to return though? The cut for each jet
    cuts = np.zeros_like(pts)
    # Ignore the overflow bin since it won't be on the plot anyways
    mask = (indices != cut70_flat.shape[0]+1)
    if verbose:
        print(indices[mask].max())
        print(cut70_flat.shape)
    cuts[mask] = cut70_flat[indices[mask]-1]

    return cuts

def plotRatio(hist_arrs, kargs_arrs, xrange=None, normed=True, nBins=100,
              logY=True, rrange=None, xlabel='', ylabel='', rlabel='Ratio',title='',
              tag='', figDir='../figures',subDir='',cuts=[]):
    '''
    Given a list of arrays, overlay the histograms and compare the
    Inputs:
    - hist_arrs:  List of numpy arrays, where the first element of the list is the
                  histogram that we're comparing the others to
    - kargs_arrs: A list of the key word arguments that will be passed to each
                  histogram to be plotted
    - xrange: The range for the xaxis of the histograms
    - normed: Whether the histograms should be normalized
    - nBins: number of bins
    - logY: whether the yaxis for the overlaid histograms should be logarithmic
    - rrange: The range on the y-axis for the ratio panel
    - xlabel: Label for the x-axis
    - ylabel: Label for the y-axis
    - title:  Title for the figure
    - tag: If not an empty string, save the file in the ../figures
    - subDir: If this (and tag) is not an empty string, save the file in this subdirectory
    - cuts: A list for the discriminant cuts for the std b-tagging WPs.
            If the default of no list is passed, cut values will not be drawn.
    '''
    assert len(hist_arrs) == len(kargs_arrs) # input arrays must be the same length

    # Define the figure with two subplots of unequal sizes
    fig = plt.figure()
    gs = gridspec.GridSpec(7,1)
    ax1 = fig.add_subplot(gs[:6,0])
    ax2 = fig.add_subplot(gs[6:,0],sharex=ax1)

    for i, (arr, kargs) in enumerate(zip(hist_arrs, kargs_arrs)):

        # Plot the histgram
        n, bins, _ = ax1.hist(arr, range=xrange, bins=nBins, normed=normed, log=logY, **kargs)

        if i == 0:
           nFirst = n
        if i > 0:
           ax2.plot((bins[:-1]+bins[1:])/2, n/nFirst, color=kargs['color'] )

    # Add a line indicating ratio=1
    ax2.plot(xrange,[1,1],'k--')
    if rrange is not None:
        ax2.set_ylim(rrange)

    # Add axes, titles and the legend
    ax1.set_ylabel('Arbitrary units')
    ax2.set_xlabel(xlabel,fontsize=14)
    ax2.set_ylabel(rlabel)
    ax1.set_title(title)
    ax1.legend()

    ylim1 = ax1.get_ylim()
    ylim2 = ax2.get_ylim()

    for cut in cuts:
        ax1.plot([cut]*2,ylim1,'grey',linestyle='--')
        ax2.plot([cut]*2,ylim2,'grey',linestyle='--')

    ax1.set_ylim(ylim1)
    ax2.set_ylim(ylim2)

    ax1.set_xlim(xrange)
    ax2.set_xlim(xrange)

    if len(tag) > 0:
        plt.savefig('{}/{}/cf_{}.pdf'.format(figDir,subDir,tag))

    plt.show()


def btagROC(beffs,leffs,ceffs,labels,title='',text='',tag='',figDir='../figures',subDir='mc16d',
            colors=None, xmin=.6,ymax=1e3,legFontSize=10,bbox_to_anchor=None,loc=None):
    '''
    Wrapper class around plotROC for the b-tagging roc curve that I make that
    overlay the b and c roc curves

    Inputs:
    - beffs: list of b-efficiencies
    - leffs: list of l-efficiencies
    - ceffs: list of c-efficiencies
    - labels: legend entries comparing the various studies
    Note: the above assume that all of these lists are the same length.
    The other inputs are the same as plotROC and are just passed directly to it.

    '''

    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(beffs))]

    bkg_effs = leffs + ceffs
    myLabels = ["{} rej: {}".format(bkg, label) \
                for bkg in ['l','c'] for label in labels]

    myStyles = ['-']*len(beffs) + ['--']*len(beffs)

    plotROC(beffs*2, leffs+ceffs, myLabels, title=title, text=text,tag=tag,figDir=figDir, subDir=subDir,
            styles=myStyles, colors=colors*2, xmin=xmin, ymax=ymax,
            legFontSize=legFontSize,bbox_to_anchor=bbox_to_anchor,loc=loc)


def plotROC(teffs, beffs, labels, title='', text='',ylabel='Background rejection',
            tag='', figDir='../figures',subDir='mc16d',
            styles=None,colors=None, xmin=.6,ymin=1,ymax=1e3,legFontSize=10,
            bbox_to_anchor=None,loc=None,leg1=None,leg2=None):
    '''
    Plot the ROC curves for a list of experiments you've run

    Inputs:
        teffs: List for the signal efficiencies
        beffs: List for the background efficiencies
        labels: List of identifiers for the experiment for the legend
        title: Title for the plot.
               If the default empty tag is passed, the plot will save some info
               about the datasample to the plot above the figure.
        tag: An option for the tag that you could append to the filename to save the plot
             The deault option of an empty tag won't produce any plots
        styles: If given, should be list of the same length as teff and beffs for the
                styles of the plots
        colors: If given, should be a list of colors to use for the plots

        xmin: The minimum value for the range on the x-axis
        ymin: The maximum value for the range on the y-axis
        legFontSize: The font size to use for the plot

    Note: This function expects the list arguments to all be the same length, but because
    of the way python's zip argument handles varying sized lists, if they aren't the same
    length it will zip to the shortest list.

    '''

    if styles is None:
        styles = ['-' for i in teffs]

    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(teffs))]

    plt.figure()
    for teff, beff, label, style, color in zip(teffs, beffs, labels, styles, colors):

        if 'rej' in ylabel:
            plt.semilogy(teff, np.divide(1,beff), style, color=color, label=label)
        else:
            plt.semilogy(teff, beff, style, color=color, label=label)

    plt.xlabel('b efficiency',fontsize=14)
    plt.ylabel(ylabel,fontsize=14)
    if len(title) > 0:
        plt.title(title)
    else:
        if len(text) == 0:
            text="$\mathbf{ATLAS}$ Simulation Internal\n"
            text+=r"$\sqrt{s}$ = 13 TeV, mc16d $t\bar{t}$"
        plt.text(xmin,ymax,text, horizontalalignment='left', verticalalignment='bottom')


    # Set the axes to be the same as those used in the pub note
    plt.xlim(xmin,1)
    plt.ylim(ymin,ymax)
    #plt.legend(loc='best',fontsize=legFontSize)
    if labels[0] is not None:
        plt.legend(fontsize=legFontSize,bbox_to_anchor=bbox_to_anchor,loc=loc)

    # Optionally, I could add functionality to create my own legends to convey
    # information more suscinctly
    if leg1 is not None:
        labels, anchor = leg1['labels'], leg1['bbox_to_anchor']
        colors, styles = leg1['colors'], leg1['styles']

        for c,s,lab in zip(colors, styles,labels):
            plt.plot([],[],color=c,linestyle=s,label=lab)
        myLeg = plt.legend(fontsize=legFontSize,bbox_to_anchor=anchor)
        ax = plt.gca().add_artist(myLeg)

    if leg2 is not None:

        labels, anchor = leg2['labels'], leg2['bbox_to_anchor']
        colors, styles = leg2['colors'], leg2['styles']

        leg2_lines = []
        for c,s,lab in zip(colors, styles,labels):
            l = plt.plot([],[],color=c,linestyle=s,label=lab)
            leg2_lines.append(l)
        plt.legend(handles=[l[0] for l in leg2_lines],fontsize=legFontSize,bbox_to_anchor=anchor)

    if len(tag) != 0:
        plt.savefig('{}/{}/roc_{}.pdf'.format(figDir,subDir,tag),bbox_inches='tight')

    plt.show()

eff_err = lambda x, N: np.sqrt( x*(1-x) / N)

def plotROCRatio(teffs, beffs, labels, title='',text='',ylabel='Background rejection',
                 tag='', figDir='../figures',subDir='mc16d',
                 styles=None,colors=None, xmin=.6,ymax=1e3,legFontSize=10,legLoc='best',
                 rrange=None, rlabel='Ratio', binomialErrors=False, nTest=0):
    '''

    Plot the ROC curves with binomial errors with the ratio plot in a subpanel
    underneath. This function all accepts the same inputs as plotROC, and the
    additional ones are listed below.

    Addtional Inputs:
    - rrange: The range on the y-axis for the ratio panel
    - rlabel: The label for the y-axis for the ratio panel
    - binomialErrors: whether to include binomial errors for the rejection curves
    - nTest: A list of the same length as beffs, with the number of events used
             to calculate the background efficiencies.
             We need this To calculate the binomial errors on the background rejection,
             using the formula given by http://home.fnal.gov/~paterno/images/effic.pdf.
    '''

    # The points that I'm going to c.f. the ratio over
    xx = np.linspace(0.6,1,101)

    if binomialErrors and nTest == 0:
        print("Error: Requested binomialErrors, but did not pass nTest. Will NOT plot rej errors.")
        binomialErrors = False

    if styles is None:
        styles = ['-' for i in teffs]
    if colors is None:
        colors = ['C{}'.format(i) for i in range(len(teffs))]

    if type(nTest) != list:
        nTest = [nTest] * len(teffs)

    # Define the figure with two subplots of unequal sizes
    fig = plt.figure(figsize=(6,5))
    gs = gridspec.GridSpec(5,1)
    ax1 = fig.add_subplot(gs[:4,0])
    ax2 = fig.add_subplot(gs[4:,0],sharex=ax1)

    for i, (teff, beff, label, style, color, nte) in enumerate(zip(teffs, beffs, labels, styles, colors, nTest)):

        # Mask the points where there was no change in the signal eff
        dx = np.concatenate((np.ones(1),np.diff(teff)))
        # Also mask the rejections that are 0
        nonzero = (beff != 0) & (dx > 0)
        x = teff[nonzero]
        y = np.divide(1,beff[nonzero])

        if binomialErrors:

            yerr = np.power(y,2) * eff_err(beff[nonzero],nte)

            y1 = y - yerr
            y2 = y + yerr

            ax1.fill_between(x,y1,y2, color=color,label=label,zorder=2) #, alpha=0.5)

        else:
            ax1.semilogy(x, y, style, color=color, label=label)

        f = pchip(x,y)

        if i == 0:
            f0 = f

            if binomialErrors:
                # Use a grey contour to show our uncertainty in the value of 1
                ax2.fill_between(x,1-yerr/y,1+yerr/y,color='grey',alpha=0.5,zorder=1)
                y0 = y
                yerr0 = yerr
        else:
            # if binomialErrors:
            #     rerr = pchip(x,(y0*yerr-y*yerr0)/np.power(y0,2))
            #     ax2.fill_between(xx, f(xx)/f0(xx)-rerr(xx), f(xx)/f0(xx)+rerr(xx), color=color)
            # else:
            ax2.plot(xx, f(xx) / f0(xx), color=color)

    # Add a line indicating where the ratio=1
    ax2.plot([xmin,1],[1,1],'k--')

    # Add axes, titles and the legend
    ax2.set_xlabel('b efficiency',fontsize=14)
    ax1.set_ylabel(ylabel,fontsize=14)
    ax1.set_yscale('log')
    ax2.set_ylabel(rlabel)
    ax1.text(xmin,ymax,text,horizontalalignment='left', verticalalignment='bottom')
    ax1.set_title(title)

    plt.setp(ax1.get_xticklabels(), visible=False)

    # Set the axes to be the same as those used in the pub note
    ax1.set_xlim(xmin,1)
    ax1.set_ylim(1,ymax)

    ax2.set_xlim(xmin,1)
    if rrange is not None:
        ax2.set_ylim(rrange)
    ax1.legend(loc=legLoc,fontsize=legFontSize)

    if len(tag) != 0:
        plt.savefig('{}/{}/rocRatio_{}.pdf'.format(figDir,subDir,tag),bbox_inches='tight')

    plt.show()



def plotInputs(X_list, y_list, dataLabels, figDir='../figures',figSubDir='mc16d/',
               trk_vars=['$s_{d0}$', '$s_{z0}$', '$p_T^{frac}$', '$\Delta R$', 'grade'],
               tag='', varRanges=[]):
    '''
    Given a list of inputs, plot the distribuitons of your normaliz
    Often times, for these studies, I've been comparing the impact of different
    datasets.

    Inputs:
    - X_list: A list of np arrays of shape nJets x nTrks x nTrkFeatures
    - y_list: A list of truth labels for the datasets in X_list
    - dataLabels: A list for the names of the the different jet distributions that we're processing
    - subDir: The subdirectory (inside figures) to save the models to
    - tag: If not an empty string, a tag for saving the models to
    '''

    defaultRanges=[(-10,10), (-10,10), (0,1), (0,8), (0,14)],
    nbins = 100
    pdgs = ['l','c','b']

    nJets, nTrks, nFeatures = X_list[0].shape
    nDatasets = len(X_list)

    if len(varRanges) == 0:
        varRanges = [defaultRanges for i in range(nDatasets)]

    fig = plt.figure(figsize=(4*nDatasets,2.5*nFeatures))

    # Loop through all of the options for the data inputs
    for i, (Xi, yi, dataLabel, var_ranges) in enumerate(zip(X_list, y_list, dataLabels,varRanges)):

        # Get the mask since all of the truth labels are track level variables
        y_trks = np.ones_like(Xi[:,:,0])
        y_trks = y_trks * yi.reshape(-1,1)

        mask_value = 0
        anti_mask = np.ones_like(Xi[:,:,0], dtype=bool)
        for feat in range(nFeatures):
            anti_mask = anti_mask & (Xi[:,:,feat] == 0)
        mask = ~ anti_mask

        y_trks = y_trks[mask]


        for j, ylabel, myRange in zip(range(nFeatures), trk_vars, var_ranges):

            idx = j*nDatasets+i+1
            ax = fig.add_subplot(nFeatures, nDatasets, idx)

            var_pdgs = [Xi[:,:,j][mask][y_trks == y] for y in range(4)]

            # Loop through the truth labels and make the histograms
            for var, l in zip(var_pdgs, pdgs):

                ax.hist(var, bins=nbins, range=myRange, histtype='step', density=True,
                        label="{}-jets".format(l))

                # Only use the log scale for the IPs
                if ('d0' in ylabel) or ('z0' in ylabel):
                    ax.set_yscale("log")

            if j == 0:
                plt.title(dataLabel)

            if i == 0:
                ax.set_ylabel(ylabel,fontsize=8)

            if (i == nDatasets-1) and (j == 0):
                ax.legend(loc='upper right',fontsize=14)

    if len(tag) > 0:
        plt.savefig("{}/{}/inputs_{}.pdf".format(figDir,subDir,tag), bbox_inches='tight')

    plt.show()


def normHist2d(c, xe, ye, normCol=True, xlabel='', ylabel='', title='',
               figName='', figDir='../figures',subDir='mc16d', cmap=None, returnCounts=False):
    '''
    Given the output from a 2d histogram, display the result either normalized
    by rows or columns

    Inputs:
    - c: a 2d np.array for the counts
    - xe: the x edges
    - ye: the x edges
    - normCol: True if you normalize by the col, pass False to normalize by row
    - xlabel: Label for the x-axis of the histogram
    - ylabel: Label for the y-axis of the histogram
    - title: Title for the figure
    - figName: Base name to save the figure to (will append rowNorm or colNorm
               as well to this name depending on normCol)
    - subDir: Subdirectory inside ../figures to save the image to
    - cmap: The color map to use for the plot
    - returnCounts: If True, return the histogram for the normalized counts that
                    is being plotted

    Note: If I divide by 0 because one of the cols (or rows),  don't have any
    entried, it will print an error message, but the output of the function
    is still fine since I just replace NaNs with 0s.

    '''

    normAxis = 1 if normCol else 0

    col_sum = np.sum(c, axis=normAxis, keepdims=True)
    c_new = c / col_sum
    c_new = np.nan_to_num(c_new)

    plt.figure()
    plt.imshow(c_new.T, origin="lower", aspect='auto',
               extent=[xe[0],xe[-1],ye[0],ye[-1]],
               cmap=cmap,norm=LogNorm())
    plt.colorbar()
    plt.xlabel(xlabel, fontsize=14)
    plt.ylabel(ylabel, fontsize=14)
    plt.title(title)

    if len(figName) > 0:
        normTag = 'colNorm' if normCol else 'rowNorm'
        plt.savefig('../figures/{}/{}_{}.pdf'.format(subDir,figName,normTag),bbox_inches='tight')

    plt.show()

    if returnCounts:
        return c_new


def TProfile(x,y,xedges=None,mask=None):
    '''

    Goal: Show the mean and the error on the mean for a variable y in bins of
    x that are specified by the variable xedges.

    '''

    assert x.size == y.size

    if xedges is None:
        xedges = np.linspace(np.min(x),np.max(y),10)
    if mask is None:
        mask = np.ones_like(x).astype(bool)

    nEdgs = xedges.shape[0]
    indices = np.digitize(x, xedges)

    mean = np.array([ y[(indices==i)&mask].mean() for i in range(1,nEdgs) ])
    err = np.array([ y[(indices==i)&mask].std() / np.sqrt(np.sum((indices==i)&mask))
           for i in range(1,nEdgs) ])

    return mean, err


def makeTable2(ms, X, X_flip, y, pt, eta, WPs=[85,60],tag='',figDir='',subDir=''):
    '''
    Plot the pT / eta performance of the flipped and nominal performance for a
    model given a set of WPs.
    '''

    if type(ms) == list:
        m_nom = ms[0]
        m_flip = ms[1]
    else:
        m_nom = ms
        m_flip = ms

    WPs=[85,60]
    algs = ['nominal','flipped' if tag=='_flip' else 'negative']

    alg_idx = ['{} {}% WP'.format(alg,WP) for WP in WPs for alg in algs ]
    eta_idx = ['$|\eta^{jet}|$ < 1.2','1.2 < $|\eta^{jet}|$ < 2.5']

    pts = [20,60,100,200,300,500] #,750] #,1000]#,3000]
    pt_idx = ['{} GeV < {} < {} GeV'.format(pt1,'$p_T^{jet}$',pt2) for pt1,pt2 in zip(pts[:-1],pts[1:])]

    alg_eta_idx = pd.MultiIndex.from_product([alg_idx,eta_idx])

    table = pd.DataFrame(0, index=alg_eta_idx, columns=pt_idx)

    # Get the discriminant for the nominal and flipped versions of the tagger
    (_, _, _), d = getEffs(m_nom, X, y, returnDisc=True, figDir=figDir,
                           subDir=subDir, modelDir=m_nom.modelDir)
    (_, _, _), d_flip = getEffs(m_flip, X_flip, y, returnDisc=True, figDir=figDir,
                                subDir=subDir, modelDir=m_flip.modelDir,tag=tag)

    nBins = 200
    myRange = (np.min(d),np.max(d))

    HF_mask = (y == 1) | (y == 2)

    # Get the WPs for the nominal version of the tagger
    for WP in WPs:

        cut = workingPoint(d[y==2], nBins, myRange, WP=.01*WP)

        print(WP,cut)

        print("nominal",np.sum(d > cut))
        print("flipped",np.sum(d_flip > cut))

        print("nominal HF",np.sum((d > cut) & HF_mask))
        print("flipped HF",np.sum((d_flip > cut) & HF_mask))

        for pt1,pt2, pt_label in zip(pts[:-1],pts[1:],pt_idx):

            for eta1,eta2, eta_label in zip([0,1.2],[1.2,2.5],eta_idx):

                pt_eta_mask = (pt > pt1) & (pt < pt2) & (np.abs(eta) > eta1) & (np.abs(eta) < eta2)

                # Get the masks for the nominal and flipped taggers
                mask_nom  = pt_eta_mask & (d > cut)
                mask_flip = pt_eta_mask & (d_flip > cut)

                # Fill in the table
                table.loc[('nominal {}% WP'.format(WP),eta_label),pt_label] = np.sum(mask_nom  & HF_mask) / np.sum(mask_nom)
                table.loc[('{} {}% WP'.format(algs[1],WP),eta_label),pt_label] = np.sum(mask_flip & HF_mask) / np.sum(mask_flip)

    return table


def effPCBins(m, X, X_flip, y, tag='',figDir='',subDir=''):
    '''
    Goal: Get the efficiencies for l,c,b in the pseudo-continuous b-tagging bins
    '''

    wp_idx = ['100%-85%','85%-77%','77%-70%','70%-60%','<60%']

    nominal = pd.DataFrame(0, index=[f+'-jets' for f in ['l','c','b']], columns=wp_idx)
    flipped = pd.DataFrame(0, index=[f+'-jets' for f in ['l','c','b']], columns=wp_idx)


    # Get the nominal discriminant
    (_, _, _), d = getEffs(m, X, y, returnDisc=True, figDir=figDir,
                           subDir=subDir, modelDir=m.modelDir)
    (_, _, _), d_flip = getEffs(m, X_flip, y, returnDisc=True, figDir=figDir,
                                subDir=subDir, modelDir=m.modelDir,tag=tag)

    nBins = 200
    myRange = (np.min(d),np.max(d))


    # Get the WPs for the nominal version of the tagger
    cuts = []
    for WP in [85,77,70,60]:

        cut = workingPoint(d[y==2], nBins, myRange, WP=.01*WP)
        cuts.append(cut)

    eps = 1e-10
    cuts = [myRange[0]-eps] + cuts + [myRange[1]+eps]

    for yi, f in enumerate(['l','c','b']):

        pdg = (y == yi)

        for cut1, cut2, wpi in zip(cuts[:-1],cuts[1:],wp_idx):

            nominal.loc[f+'-jets', wpi] = np.sum((d > cut1) & (d < cut2) & pdg) / np.sum(pdg)
            flipped.loc[f+'-jets', wpi] = np.sum((d_flip > cut1) & (d_flip < cut2) & pdg) / np.sum(pdg)

    return nominal,flipped
