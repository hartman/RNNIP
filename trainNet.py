'''
trainNet.py

Script for training the different networks with enough functionality to
save the different hyperparameters.

There's an option parser that will run the script using these commands if this is run as the main
script, but the class can be loaded in as a module to interact more directly with the
training in a notebook.

Nicole Hartman, Winter 2018

'''

import numpy as np
import h5py
import os
import sys

from usefulFcts import *
#from recurrent_attention_machine_translation import DenseAnnotationAttention

from keras import backend as K
from keras.models import Model, load_model
from keras.layers import Layer, Masking, Input, Dense, Dropout, LSTM, concatenate, Multiply
from keras.layers import LSTMCell, GRU, GRUCell, Bidirectional, RNN, ReLU, BatchNormalization
from keras.layers import BatchNormalization, Embedding, Lambda, TimeDistributed
from keras.callbacks import EarlyStopping, ModelCheckpoint
from keras.utils.np_utils import to_categorical
import time

import h5py
import sys

#from EnergyFlow.energyflow.archs import PFN
#sys.path.append("EnergyFlow")
#from energyflow.archs.efn import PFN
N_CATEGORIES=14

class Sum(Layer):
    """
    Simple sum layer.
    The tricky bits are getting masking to work properly, but given
    that time distributed dense layers _should_ compute masking on their
    own.

    Author: Dan Guest
    https://github.com/dguest/flow-network/blob/master/SumLayer.py

    """

    def __init__(self, axis=1, keepdims=False, **kwargs):
        super().__init__(**kwargs)
        self.supports_masking = True
        self.axis = axis
        self.keepdims = keepdims

    def build(self, input_shape):
        pass

    def call(self, x, mask=None):
        if mask is not None:
            x = x * K.cast(mask, K.dtype(x))[:,:,None]
        return K.sum(x, axis=self.axis, keepdims=self.keepdims)

    def compute_output_shape(self, input_shape):
        return input_shape[0], input_shape[2]

    def compute_mask(self, inputs, mask):
        return None



class myModel():
    '''
    The basic base class for the models that I'm interested in training / comparing

    Should have the basic attributes for the LSTM, and the optimizer / fitting functions

    '''

    def __init__(self, timeSteps=15, nFeatures=5, loadModel=False, modelDir='models', dataTag="", nClasses=4):
        '''

  	    '''

        print("Initialize method from myModel()")

        self.timeSteps = timeSteps
        self.nFeatures = nFeatures
        self.loadModel = loadModel
        self.modelDir = modelDir
        self.dataTag = dataTag
        self.modelName = "please_overwrite_me"
        self.doEmbedding = False
        self.nClasses = nClasses

        # Attibutes that should be overwritten in the children classes
        self.model = None
        self.hist = None
        self.monitor = None
        self.save_weights_only = None

    def createModel(self):
        '''
	    This method will be implemented by the children inheriting from this class
        '''
        pass

    def train(self, X_train, y_train_cat, weights_train, CV_split=0.2,
              nEpochs=50, loss_weights=None, X_jet_train=None):
        '''
        Define the optimizer and train the model.

        It saves the best model to an hdf5 file specified by the self.modelName attribute.
        (Ideally, it would also be able to load in models to continue a previous training -
         still need to implement this)

        Inputs:
            X_train, y_train_cat: Inputs and truth labels (as one-hot vectors) for the train set
            weights_train: The sample weights from the pT reweighting
            CV_split: The percent of the data to hold out for the validation curves
            nEpochs: number of epochs to train for
            X_jet_train: If not None, accepts another training stream for the jet
                         inputs to the model, to be concatenated with the lstm's hidden
                         state before applying the dense layer before classification.

        '''

        # Check that the inputs have the correct dimensionality
        _, ts, nFeat = X_train.shape
        if ts != self.timeSteps or nFeat != self.nFeatures:
            print("Error: X_train does not have the correct dimensionality for the model.")
            return

        # Check that the model has been properly instantiated
        if self.model is None:
            print("Error: This function should not be called from parent class myModel().")
            return

        # Define the optimizer
        self.model.compile(loss='categorical_crossentropy', optimizer='adam',
                           metrics=['acc'], loss_weights=loss_weights)

        # Get the relevant inputs
        if self.doEmbedding:
            print("Training with the embedding layer")
            my_input = [X_train[:,:,:-1],X_train[:,:,-1]]
            assert X_jet_train is None # Haven't implemented a jet stream with the embedding functionality
        elif X_jet_train is None:
            my_input = X_train
        else:
            my_input = [X_train,X_jet_train]


        mTag = '_weights.h5' if self.save_weights_only else '.hdf5'
        mLoc =  '{}/{}{}'.format(self.modelDir,self.modelName,mTag)
        print('model checkpoint: {}'.format(mLoc))

        monitor = self.monitor
        verbose =0
        earlyStop = EarlyStopping(monitor=monitor, verbose=verbose, patience=10)
        #,restore_best_weights=False)
        mChkPt = ModelCheckpoint(mLoc,monitor=monitor, verbose=verbose,
                                 save_best_only=True,
                                 save_weights_only=self.save_weights_only)

	    # Fit the model
        self.hist = self.model.fit(my_input, y_train_cat,
                                   epochs=nEpochs, batch_size=256, verbose=1,
                                   validation_split=CV_split,
                                   callbacks=[earlyStop, mChkPt],
                                   sample_weight=weights_train)

		# Save the history object as well
        h = h5py.File('{}/{}_history.hdf5'.format(self.modelDir, self.modelName), 'w')

        # Save the weights as well!
        # This was before I started using the save_weights_only flag ModelCheckpoint
        #self.model.save_weights('{}/{}'.format(self.modelDir,weightFile))

        # Concatenate with the previous history object if you're training from a warm start
        '''
        if self.loadModel:
            print("Loading in the previous loss and acc training curves")

            assert loss_weights is None # Only implemented load_model for retraining w/o aux loss

            loss, val_loss, acc, val_acc = self.loadHistory()

            h.create_dataset("loss",     data=np.append(loss,    self.hist.history['loss']))
            h.create_dataset("val_loss", data=np.append(val_loss,self.hist.history['val_loss']))
            h.create_dataset("acc",      data=np.append(acc,     self.hist.history['acc']))
            h.create_dataset("val_acc",  data=np.append(val_acc, self.hist.history['val_acc']))

        else:
        '''
        for key, val in self.hist.history.items():
            h.create_dataset(key, data=val)

        print("Saving history object")
        h.close()

    def eval(self, X):
        '''
        Do the preprocessing of the inputs here since
        with the embedding layer and the functional api, I need to pass
        the inputs as two separate streams
        '''
        if self.doEmbedding:
            return self.model.predict([X[:,:,:-1],X[:,:,-1]], batch_size=256, verbose=0)

        else:
	        return self.model.predict(X, batch_size=256, verbose=0)


    def loadHistory(self):
        '''
        When you're retraining from a warm start, load in the previous training
        loss and accuracies and concatenate them with the new ones before saving
        the final history object.
        '''

        g = h5py.File("{}/{}_history.hdf5".format(self.modelDir, self.modelName),"r")

        loss     = g['loss'][:]
        val_loss = g['val_loss'][:]
        acc      = g['acc'][:]
        val_acc  = g['val_acc'][:]

        return loss, val_loss, acc, val_acc

    def processInputs(self):
        '''
        Process the separate trk stream inputs when doing the grade embedding
        '''
        cts_trk_inputs = Input(shape=(self.timeSteps,self.nFeatures-1),name="Continuous_trk_inputs")
        masked_input = Masking()(cts_trk_inputs)

        # Next look at a 2d embedding layer
        grade_input = Input(shape=(self.timeSteps,),name="Categorical_trk_inputs", dtype='int32')
        embed = Embedding(N_CATEGORIES+1,2,mask_zero=True,input_length=self.timeSteps)(grade_input)

        # Merge the two layers
        myMerge = concatenate([masked_input, embed])

        trkEmbeddedInputs = Model(inputs=[cts_trk_inputs,grade_input],outputs=myMerge)
        return trkEmbeddedInputs


    def get_lstm_out(self,x):
        '''
        When I return the lstm as a sequence, it's shape is
        (batch size, max # of timesteps, # hidden neurons)

        So this function just returns the last timestep of
        the sequence.
        '''

        return x[:,-1,:]

class myLSTM(myModel):

    '''
    Class for training the LSTM baselines
    '''

    def __init__(self, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, nJetVars=0,
                 loadModel=False,modelDir='models',
                 dataTag='', nClasses=4, modelName=''):
        myModel.__init__(self, timeSteps, nFeatures, loadModel, modelDir, dataTag, nClasses)

        self.nHidden = nHidden
        self.nDense = nDense
        self.nJetVars = nJetVars

        self.catTag = "_gradeEmbed" if doEmbedding else ""
        self.doEmbedding = doEmbedding

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:
            self.modelName = 'LSTM_{}'.format(self.modelTag())
        else:
            self.modelName = modelName

        self.monitor = 'val_loss'
        self.save_weights_only = False

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        if self.loadModel and os.path.isfile(modelLoc):
            print("Loading previously trained LSTM")
            self.model = load_model(modelLoc)
            # Still need to load the history objects here as well

        else:
            print("Building the baseline LSTM")

            if self.doEmbedding:
                cts_trk_inputs = Input(shape=(timeSteps,nFeatures-1),name="Continuous_trk_inputs")
                grade_input = Input(shape=(timeSteps,),name="Categorical_trk_inputs", dtype='int32')
                trk_inputs=[cts_trk_inputs, grade_input]
                masked_input = self.processInputs()(trk_inputs)
            else:
                trk_inputs = Input(shape=(timeSteps,nFeatures),name="Trk_inputs")
                masked_input = Masking()(trk_inputs)

            # Feed this merged layer to an RNN
            lstm = LSTM(nHidden, return_sequences=False, name='LSTM')(masked_input)
            dpt = Dropout(rate=0.2)(lstm)

            if nJetVars != 0:
                print("{} jet vars".format(nJetVars))
                jet_inputs = Input(shape=(nJetVars,),name="Jet_inputs")
                dpt = concatenate([dpt,jet_inputs])
                my_inputs = [trk_inputs,jet_inputs]
            else:
                my_inputs = trk_inputs


            # Fully connected layer
            if nDense != 0:
                FC = Dense(nDense, activation='relu', name="Dense")(dpt)
            else:
                FC = dpt

            # Softmax for classification
            output = Dense(self.nClasses, activation='softmax', name="Jet_class")(FC)
            self.model = Model(inputs=my_inputs, outputs=output)

    def modelTag(self):
        '''
	    Get the tag for the model architecture using the class parameters that
        is common to all of the models that we're training
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''
        outTag = '_{}out'.format(self.nClasses) if self.nClasses != 4 else ''
        archTag = '{}units{}{}_{}{}'.format(self.nHidden, denseTag, outTag,
                                          self.dataTag, self.catTag)

        return archTag

class LSTM_trkClass(myModel):

    '''
    Class for training the LSTM baselines
    '''

    def __init__(self, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, loadModel=False,modelDir='models',
                 dataTag='', nClasses=4, modelName='', nDenseTrk=0, alpha=0.5, nTrkClasses=5):
        myModel.__init__(self, timeSteps, nFeatures, loadModel, modelDir, dataTag, nClasses)

        self.nHidden = nHidden
        self.nDense = nDense
        self.catTag = "_gradeEmbed" if doEmbedding else ""

        self.doEmbedding = doEmbedding

        self.nDenseTrk = nDenseTrk

        print("alpha=",alpha)

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:
            trkClassTag = "_" if nTrkClasses==5 else "_{}_".format(nTrkClasses)
            self.modelName = 'LSTM{}trkClass_{}_alpha{:1.1f}'.format(trkClassTag,self.modelTag(),alpha)
        else:
            self.modelName = modelName

        self.monitor = 'val_Jet_class_loss'
        self.save_weights_only = True

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        weightFile = modelDir + "/" + self.modelName + "_weights.h5"

        # atm, not supporting embedding for this class
        assert doEmbedding == False

        trk_inputs = Input(shape=(timeSteps,nFeatures),name="Trk_inputs")
        masked_input = Masking()(trk_inputs)

        # Feed this merged layer to an RNN
        lstm = LSTM(nHidden, return_sequences=True,name="LSTM")(masked_input)
        lstm_out = Lambda(self.get_lstm_out, output_shape=(nHidden,))(lstm)

        if nDenseTrk != 0:
            trk_vec = TimeDistributed(Dense(nDenseTrk))(lstm)
        else:
            trk_vec = lstm
        trk_out = TimeDistributed(Dense(nTrkClasses, activation='softmax'),name="Trk_class")(trk_vec)

        dpt = Dropout(rate=0.2)(lstm_out)

        # Fully connected layer
        FC = Dense(nDense, activation='relu')(dpt)

        # Softmax for classification
        output = Dense(self.nClasses, activation='softmax',name="Jet_class")(FC)
        self.model = Model(inputs=trk_inputs, outputs=[output,trk_out])


        if self.loadModel and os.path.isfile(weightFile):
            print("Loading weights for previously trained model")
            self.model.load_weights(weightFile)



    def modelTag(self):
        '''
	    Get the tag for the model architecture using the class parameters that
        is common to all of the models that we're training
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''
        outTag = '_{}out'.format(self.nClasses) if self.nClasses != 4 else ''

        archTag = '{}units{}{}_'.format(self.nHidden, denseTag,outTag)
        archTag += "{}trkDense_".format(self.nDenseTrk) if self.nDenseTrk != 0 else ""
        archTag += '{}{}'.format(self.dataTag, self.catTag)

        return archTag

    def eval(self, X, trk_out=False):
        '''
        Overwrite the parent class since this model has multiple outputs!

        Also, this class does not support embedding

        Inputs:
        - X: The test set to evaluate the model on
        - trk_out: The part model we want to evaluate the performance on
            * If False (default), look at the output of the jet classier, and
              output will have shape nJets, nJetClasses
            * If True, look at the output of the track classier, so the output
              will have shape nJets, timeSteps, nTrkClasses

        '''
        out = self.model.predict(X, batch_size=256, verbose=0)
        if trk_out:
            return out[1]
        else:
            return out[0]


class NMT(myModel):

    '''
    Class for training the NMT model for jointly aligning and translating for
    this classification problemself.

    There are several knobs I can turn on this model so I'll do my best to
    make it general enough to permit experimentation!

    '''

    def __init__(self, nHidden=50, nDense=10, doEmbedding=False,
                 timeSteps=15, nFeatures=5, loadModel=False,modelDir='models',
                 dataTag='', nClasses=4, modelName='', encUnits=50,attnUnits=25):
        myModel.__init__(self, timeSteps, nFeatures, loadModel, modelDir, dataTag, nClasses)

        self.nHidden = nHidden
        self.nDense = nDense
        self.catTag = "_gradeEmbed" if doEmbedding else ""

        self.doEmbedding = doEmbedding

        # Passing modelName as an input allows differing functionality from
        # training to testing as I'm developing
        if len(modelName) == 0:

            encTag = "enc_bidir_GRU_{}units".format(encUnits)
            decTag = "dec_{}attUnits_GRU_{}".format(attnUnits,self.modelTag())
            self.modelName = 'NMT_{}_{}'.format(encTag, decTag)

        else:
            self.modelName = modelName

        self.monitor = 'val_loss'
        self.save_weights_only = True

        assert not self.doEmbedding
        trk_inputs = Input((timeSteps,nFeatures), name="input_sequences")
        masked_inputs = Masking()(trk_inputs)

        encoder_rnn = Bidirectional(GRU(encUnits,
                                    return_sequences=True,
                                    return_state=True))
        x_enc, h_enc_fwd_final, h_enc_bkw_final = encoder_rnn(masked_inputs)

        # the final state of the backward-GRU (closest to the start of the input
        # sentence) is used to initialize the state of the decoder
        initial_state_gru = Dense(nHidden, activation='tanh')(h_enc_bkw_final)
        initial_attention_h = Lambda(lambda x: K.zeros_like(x)[:, 0, :])(x_enc)
        initial_state = [initial_state_gru, initial_attention_h]

        cell = DenseAnnotationAttention(cell=GRUCell(nHidden),
                                        units=attnUnits)
        # TODO output_mode="concatenate", see TODO(3)/A
        decoder_rnn = RNN(cell=cell, return_sequences=True, return_state=True)
        h1_and_state = decoder_rnn(masked_inputs, initial_state=initial_state, constants=x_enc)
        h1 = h1_and_state[0]

        lstm_out = Lambda(self.get_lstm_out, output_shape=(nHidden,))(h1)

        dpt = Dropout(rate=0.2)(lstm_out)

        # Fully connected
        FC = Dense(nDense, activation='relu')(dpt)

        # Softmax for classification
        output = Dense(self.nClasses, activation='softmax',name="Jet_class")(FC)

        self.model = Model(trk_inputs, output)

        weightFile = '{}/{}_weights.h5'.format(modelDir,self.modelName)
        if self.loadModel and os.path.isfile(weightFile):
            print("Loading weights for previously trained model")
            self.model.load_weights(weightFile)

    def modelTag(self):
        '''
	    Get the model tag defining this architecture
        '''

        nDense = self.nDense
        denseTag = '_{}dense'.format(nDense) if nDense != 0 else ''
        outTag = '_{}out'.format(self.nClasses) if self.nClasses != 4 else ''

        archTag = '{}units{}{}_{}{}'.format(self.nHidden, denseTag, outTag, self.dataTag, self.catTag)

        return archTag


class myPFN(myModel):

    def __init__(self, ppm_sizes, dense_sizes, latent_dropout=0,
                 timeSteps=15, nFeatures=5, loadModel=False, modelDir='models',
                 dataTag="",nClasses=4,modelName=""):
        myModel.__init__(self, timeSteps, nFeatures, loadModel, modelDir, dataTag, nClasses)

        if type(ppm_sizes[0]) == str:
            ppm_sizes_str = ppm_sizes
            ppm_sizes_int = [int(n) for n in ppm_sizes]
        else:
            ppm_sizes_str = [str(n) for n in ppm_sizes]
            ppm_sizes_int = ppm_sizes

        if type(dense_sizes[0]) == str:
            dense_sizes_str = dense_sizes
            dense_sizes_int = [int(n) for n in dense_sizes]
        else:
            dense_sizes_str = [str(n) for n in dense_sizes]
            dense_sizes_int = dense_sizes

        if len(modelName) == 0:

            phiTag =  "_".join(ppm_sizes_str)
            dptTag = "_dpt_0p{}".format(latent_dropout*100) if latent_dropout != 0 else ""
            FTag =  "_".join(dense_sizes_str)

            outTag = '_{}out'.format(self.nClasses) if self.nClasses != 4 else ''
            self.modelName = 'PFN_phi_{}{}_F_{}{}_{}'.format(phiTag,dptTag,FTag,outTag,dataTag)

        else:
            self.modelName = modelName

        self.monitor = 'val_loss'
        self.save_weights_only = False

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        if self.loadModel and os.path.isfile(modelLoc):
            print("Loading previously trained PFN")
            self.model = load_model(modelLoc)
            # Still need to load the history objects here as well

        else:
            print("Building the baseline PFN")

            # Just use the pfn model from the package provided with the paper
            pfn = PFN(input_dim=nFeatures,
                      ppm_sizes=ppm_sizes_int,
                      dense_sizes=dense_sizes_int,
                      output_dim=self.nClasses, latent_dropout=latent_dropout)
            self.model = pfn.model # keras model

def getMask(x):
    mask = K.cast(K.any(x, axis=-1, keepdims=True),'float32')
    return mask

def compute_output_shape(input_shape):
    return input_shape

class DIPS(myModel):

    def __init__(self, ppm_sizes, dense_sizes, dropout=0,
                 timeSteps=15, nFeatures=5, loadModel=False, modelDir='models',
                 dataTag="",nClasses=4,modelName="",batch_norm=False,
                 weightFile='',modelTag='',attn=False, neg=False):
        myModel.__init__(self, timeSteps, nFeatures, loadModel, modelDir, dataTag, nClasses)
        '''
        Goal: Patrick's EnergyFlow network was nice, but we really want something
        that is easier to plug into LWTNN, and that is more transparent for the
        different experiments that we want to run.

        Dan Guest shared a repo with me where he made a Keras custom sum layer,
        and showed how to implement a minimal working dataset that I just need
        to modify for my framework.

        https://github.com/dguest/flow-network/blob/master/train_nn.py

        '''

        if type(ppm_sizes[0]) == str:
            ppm_sizes_str = ppm_sizes
            ppm_sizes_int = [int(n) for n in ppm_sizes]
        else:
            ppm_sizes_str = [str(n) for n in ppm_sizes]
            ppm_sizes_int = ppm_sizes

        if type(dense_sizes[0]) == str:
            dense_sizes_str = dense_sizes
            dense_sizes_int = [int(n) for n in dense_sizes]
        else:
            dense_sizes_str = [str(n) for n in dense_sizes]
            dense_sizes_int = dense_sizes

        if len(modelName) == 0:

            phiTag =  "_".join(ppm_sizes_str)
            dptTag = "_dpt_{}".format(dropout) if dropout != 0 else ""
            bnTag = "_bn" if batch_norm else ""
            attnTag = "_attn" if attn else ""
            FTag =  "_".join(dense_sizes_str)

            outTag = '_{}out'.format(self.nClasses) if self.nClasses != 4 else ''
            self.modelName = 'DIPS_phi_{}_F_{}{}{}{}{}_{}{}'.format(phiTag,FTag,outTag,dptTag,bnTag,attnTag,dataTag,modelTag)

        else:
            self.modelName = modelName

        self.monitor = 'val_loss'
        self.save_weights_only = True

        trk_inputs = Input(shape=(timeSteps,nFeatures))
        masked_inputs = Masking(mask_value=0)(trk_inputs)
        tdd = masked_inputs


        for i, phi_nodes in enumerate(ppm_sizes_int):

            tdd = TimeDistributed(Dense(phi_nodes,activation='linear'),name="Phi{}_Dense".format(i))(tdd)
            if batch_norm:
                tdd = TimeDistributed(BatchNormalization(),name="Phi{}_BatchNormalization".format(i))(tdd)
            if dropout != 0:
                tdd = TimeDistributed(Dropout(rate=dropout),name="Phi{}_Dropout".format(i))(tdd)
            tdd = TimeDistributed(ReLU(),name="Phi{}_ReLU".format(i))(tdd)

        # Pooling operation
        if attn:

            # I'm not sure if I need more than one layer here or not?
            tws = TimeDistributed(Dense(ppm_sizes_int[0],activation='tanh'),   name="Alpha1_Dense".format(i))(masked_inputs)
            u = TimeDistributed(Dense(ppm_sizes_int[-1],activation='linear'),name="Alpha2_Dense".format(i))(tws)
            #u = TimeDistributed(Dense(1,activation='linear'),name="Alpha2_Dense".format(i))(tws)

            # Normalize the attention weights
            num = Lambda(K.exp, output_shape=compute_output_shape)(u)
            # Mask the values
            num = Lambda(lambda x: x[0] * getMask(x[1]), output_shape=compute_output_shape)([num,tdd])

            den = Sum(axis=1,keepdims=True)(num)
            den = Lambda(K.pow, output_shape=compute_output_shape, arguments={'a':-1.})(den)

            alpha = Multiply(name="Attention_Softmax")([num,den])

            # This is where the magic happens... weighted sum up the track features!
            F = Multiply()([alpha,tdd])
            F = Sum()(F)

        else:
            # This is where the magic happens... sum up the track features!
            F = Sum()(tdd)

        # For the negative version of the alg, we could try multiplying the output of this sum layer by 2?
        if neg:
            F = Lambda(lambda x: x * 2, output_shape=compute_output_shape)(F)

        for j, (F_nodes, p) in enumerate(zip(dense_sizes_int,
                                         [dropout]*len(dense_sizes_int[:-1])+[0])):

            F = Dense(F_nodes, activation='linear', name="F{}_Dense".format(j))(F)
            if batch_norm:
                F = BatchNormalization(name="F{}_BatchNormalization".format(j))(F)
            if dropout != 0:
                F = Dropout(rate=p,name="F{}_Dropout".format(j))(F)
            F = ReLU(name="F{}_ReLU".format(j))(F)

            #prevDim = F_nodes

        output = Dense(self.nClasses, activation='softmax',name="Jet_class")(F)
        self.model = Model(inputs=trk_inputs, outputs=output)

        modelLoc = '{}/{}.hdf5'.format(modelDir,self.modelName)
        if len(weightFile) == 0:
            weightFile = modelDir + "/" + self.modelName + "_weights.h5"
        print("weightFile",weightFile)
        if self.loadModel and os.path.isfile(weightFile):
            print("Loading previously trained DIPS weight file")
            self.model.load_weights(weightFile)
        else:
            print('Starting from new weight initialization')


if __name__ == '__main__':

    from argparse import ArgumentParser

    '''
    Load in the options from the command line
    '''
    p = ArgumentParser()

    p.add_argument('--nEpoch','--nEpochs', type=int, default=200, dest='nEpoch',
                   help = 'number of epochs (default 200)')
    p.add_argument('--nMaxTrack','--nTrks', type=int, default=15, dest="nMaxTrack",
                   help="Maximum number of tracks")
    p.add_argument('--nLSTMNodes', type=int, default=50, dest="nLSTMNodes",
                   help="number of hidden nodes for the LSTM algorithm")
    p.add_argument('--nFCNodes', type=int, default=10, dest="nFCNodes",
                   help="number of hidden nodes for the FC layer before classification")
    p.add_argument('--doEmbedding', action='store_true', dest="doEmbedding",
                   help="Whether you should embed the track category")
    p.add_argument('--nFixedTracks', '--nFixedTrks', type=int, default=-1, dest="nFixedTrks",
                   help="Whether you're only selecting jets with a fixed # of tracks")
    p.add_argument('--useTrackless',action="store_true",
                   help='Include jets w/o any tracks in the training')
    p.add_argument('--nClasses', type=int, default=3, dest="nClasses",
                   help="Number of output classes to use for the model.")
    p.add_argument('--model', type=str, default='LSTM', dest="model",
                   help="Type of model: LSTM, LSTM_trkClass, NMT, PFN, DIPS")
    p.add_argument('--iter',type=int, default=-1,help='For ensembling / timing studies to train multiple networks with the same config')

    # ---- extra args for the LSTM_trkClass model ----
    p.add_argument('--alpha', type=float, default=0.5, dest="alpha",
                   help="The weight to put on the trk classification loss: L = Ljet + alpha Ltrk (default alpha=0.5)")
    p.add_argument('--nDenseTrk', type=int, default=0, dest="nDenseTrk",
                   help="number of hidden nodes for an int layer from the LSTM seq -> trk classification")
    p.add_argument('--nTrkClasses', type=int, default=0, dest='nTrkClasses', help="# of trk classes")

    # ---- extra args for the NMT model ----
    p.add_argument('--nEncNodes', type=int, default=50, dest="nEncNodes",
                   help="number of hidden nodes for the encoder RNN")
    p.add_argument('--nDecNodes', type=int, default=50, dest="nDecNodes",
                   help="number of hidden nodes for the decoder RNN")
    p.add_argument('--nAttnNodes', type=int, default=25, dest="nAttnNodes",
                   help="number of nodes for the mlp getting the weights for the attention mechanism")

    # ---- extra args for the DeepSets model ----
    p.add_argument('--ppm_sizes', type=str, default='100,100,128',dest='ppm_sizes',
                   help='Sizes for the per particle network in deep sets model')
    p.add_argument('--dense_sizes', type=str, default='100,100,100',dest='dense_sizes',
                   help='Sizes for the dense network (after trk feature summation) in deep sets model')
    p.add_argument('--latent_dropout', type=float, default=0,dest='latent_dropout',
                   help='If nonzero, the fraction of nodes to drop in the track latent dim for the deep sets model')

    # ---- extra args for DIPS model, the ppm_sizes and dense_sizes are still used ----
    p.add_argument('--dropout', type=float, default=0,dest='dropout',
                   help='If nonzero, the fraction of nodes to drop for every layer, except the last one')
    p.add_argument('--batch_norm', action="store_true",
                   help='Whether to use batch norm in the DIPS layers')
    p.add_argument('--attention', action="store_true",
                   help='Whether to use an attention mechanism in the pooling operation')

    p.add_argument('--loadModel', action='store_true', dest="loadModel",
                   help="Whether you retrain or load a previously trained model")
    p.add_argument('--weightFile', type=str, dest="weightFile", default='',
                   help="If passed, the weight file to load in for the model parameters.")
    p.add_argument('--modelTag', type=str, dest='modelTag',default='',
                   help="Extra tag to append to the model name in case of ambiguity.")

    p.add_argument('--nJets', type=int, default=3e6, dest="nJets",
                   help="Numer of jets in the dataset (default 3m)")
    p.add_argument('--mode', type=str, default='',
                   help='Mode the processing data was processed (for accessing data): either train or empty string')
    p.add_argument('--rwtDist',type=str,default='l',dest="rwtDist",
                   help="The flavor spectrum to reweight the pT to: l (default), c, or b")

    p.add_argument('--dataTag', type=str, default='', dest='dataTag',
                   help="If you pass this argument, it will load in the file "\
                       +"'data_'+dataTag instead of using the other individual "\
                       +"arguments, and use this flag to name + save the trained model.")

    p.add_argument('--trkIPSign','--trkIPSgn', type=str, default='all', dest="trkIPSgn",
                   help="The sign of the IP to select for the tracks: all (default), pos / positive, or neg / negative")
    p.add_argument('--sortFlag', type=str, dest="sortFlag", default="sd0_rev",
                   help="Sorting configuration for the output dfs. Available options: sd0_rev (default), sd0_absrev, sd0_negrev, noSort")

    p.add_argument('--trkSelection',type=str,dest="trkSelection",default='',
                   help="The track selection to use: the default is the same "\
                   +"as ip3d, but a string like pt_700_d0_2_z0sinTheta_6 "\
                   +"will be interpretted as pT > 700 MeV, |d0| < 2 mm, "\
                   +"|z0*sin(theta)| < 6 mm")

    p.add_argument('--noNormVars', type=str, default='sd0,sz0,nNextToInnHits,nInnHits,'\
                   +'nsharedBLHits,nsplitBLHits,nsharedPixHits,nsplitPixHits,nsharedSCTHits',
                   help='Variables not to normalize: Pass -1 for an empty list')
    p.add_argument('--logNormVars', type=str, default='ptfrac, dr',
                   help='Variables to take the log of before normalizing, '\
                   +'default ptfrac, dr: Pass -1 for an empty list')
    p.add_argument('--jointNormVars', type=str, default='nPixHits,nSCTHits',
                   help='Variables to whiten: Pass -1 for an empty list')
    p.add_argument('--jetVars',type=str, default="-1", help="Jet variables to pass to the network")

    p.add_argument("--mc", type=str, default="mc16d", dest="mc",
                   help="mc version: mc16a, mc16d (default), mc16e")
    p.add_argument('--jetCollection', type=str, default='Topo', dest="jetCollection",
                   help="Jet collection to train over: Topo (default) or PFlow")
    p.add_argument('--physicsSample', type=str, default='ttbar', dest="physicsSample",
                   help="the physics sample of interest: can be one of ttbar (default),"
                   +"Zprime_1.5TeV, Zprime_5TeV, hybrid_1.5TeV, hybrid_5TeV")

    args = p.parse_args()

    '''
    Step 1: Load in the training and test sets
    '''
    # Access the max length for the number of time steps for training the net
    jetTag = nJetsTag(args.nJets)
    jetTag += args.mode
    rwtTag = '' if args.rwtDist == 'l' else '_rwt-{}'.format(args.rwtDist)
    jetVars = strToList(args.jetVars)
    nJetVars = len(jetVars)
    if len(jetVars) > 0:
        jetTag += "_{}".format("_".join(jetVars))

    if args.nFixedTrks == -1:
        jetTag += "_{}trks".format(args.nMaxTrack)
        timeSteps = args.nMaxTrack
    else:
        jetTag += "_ONLY_{}trks".format(args.nFixedTrks)
        timeSteps = args.nFixedTrks

    # myTrkSelection = "_"+args.trkSelection if len(args.trkSelection) > 0 else args.trkSelection
    # jetTag += myTrkSelection

    # Get the tag to select the number of jets and # of trks
    if len(args.dataTag) == 0:

        sortFlag = args.sortFlag
        ipTag = getIPTag(args.trkIPSgn)

        iterTag = "" if args.iter == -1 else f"_iter{args.iter}"

        # Get a string representing the variables fed into this net
        noNormVars = strToList(args.noNormVars)
        logNormVars = strToList(args.logNormVars)
        jointNormVars = strToList(args.jointNormVars)

        varTag = "_".join(noNormVars)
        varTag += '_logNorm_' + "_".join(logNormVars) if len(logNormVars) != 0 else ""
        varTag += '_norm_' + "_".join(jointNormVars) if len(jointNormVars) != 0 else ""

        nFeatures = len(noNormVars + logNormVars + jointNormVars)

        dataTag = '{}_{}_{}{}{}'.format(jetTag, varTag, sortFlag, ipTag, rwtTag)

    else:
        dataTag = args.dataTag
        nFeatures=5 if 'grade' in dataTag else 13

    subDir = "{}_{}_{}".format(args.mc,args.jetCollection,args.physicsSample)
    if len(args.trkSelection) > 0:
        subDir += "_{}".format(args.trkSelection)

    #trkOrigTag = '_trkOrig' if args.model == 'LSTM_trkClass' else ''
    #if len(trkOrigTag) > 0:
    #    trkOrigTag += '' if nTrkClasses == 5 else "_{}trkClasses".format(nTrkClasses)

    nTrkClasses = args.nTrkClasses
    trkOrigTag = '_{}trkClasses'.format(nTrkClasses) if nTrkClasses != 0 else ''
    inputFile = 'data/{}/data_{}{}.hdf5'.format(subDir,dataTag,trkOrigTag)

    print("Attempting to open:",inputFile)
    f = h5py.File(inputFile,"r")

    # https://stackoverflow.com/questions/20928136/input-and-output-numpy-arrays-to-h5py
    X_train       = f['X_train'][:]
    y_train       = f['y_train'][:]
    ix_train      = f['ix_train'][:]
    weights_train = f['weights_train'][:]
    if nJetVars > 0:
        print('Loading in jet vars',jetVars)
        X_jet_train = f['X_jet_train'][:]
    else:
        X_jet_train = None

    if not os.path.exists('models/'+subDir):
        print('Making directory ','models/'+subDir)
        os.mkdir('models/'+subDir)

    # Make the one-hot vectors
    if y_train.max() >= args.nClasses:

        valid_targets = (y_train < args.nClasses)

        X_train       = X_train[valid_targets]
        y_train       = y_train[valid_targets]
        ix_train      = ix_train[valid_targets]
        weights_train = weights_train[valid_targets]

    # Remove the trackless jets
    if not args.useTrackless:
        nTrks = np.sum(~ np.all(X_train == 0, axis=-1),axis=-1)
        X_train       = X_train[nTrks != 0] 
        y_train       = y_train[nTrks != 0] 
        ix_train      = ix_train[nTrks != 0] 
        weights_train = weights_train[nTrks != 0] 

    y_train_cat = to_categorical(y_train, num_classes=args.nClasses)

    '''
    Step 2: Load in the correct model
    '''
    modelDir = "models/" + subDir

    # I also want the functionality to save multiple trainings for the same configuration
    dataTag += iterTag
    
    if args.model == "LSTM":
        print("Loading LSTM model")
        m = myLSTM(nHidden=args.nLSTMNodes, nDense=args.nFCNodes, doEmbedding=args.doEmbedding,
                   timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                   nFeatures=nFeatures, nJetVars=nJetVars, dataTag=dataTag, nClasses=args.nClasses)

        my_output = y_train_cat
        my_weights = weights_train

    elif args.model == "LSTM_trkClass":

        m = LSTM_trkClass(nHidden=args.nLSTMNodes, nDense=args.nFCNodes,
                          doEmbedding=args.doEmbedding, timeSteps=timeSteps,
                          loadModel=args.loadModel, modelDir=modelDir,
                          nFeatures=nFeatures, dataTag=dataTag, nClasses=args.nClasses,
                          nDenseTrk=args.nDenseTrk, alpha=args.alpha,
                          nTrkClasses=nTrkClasses)

        y_trk_train = f['y_trk_train'][:]
        nTrks_train = f['nTrks_train'][:]
        my_output = [ y_train_cat, to_categorical(y_trk_train,num_classes=nTrkClasses) ]

        # Need to make sure we're not dividing by 0 for the trackless jets,
        # otherwise we'd get NaN losses + the track classifer model can't train
        trk_weights = weights_train.copy()
        trk_weights[nTrks_train != 0] /= nTrks_train[nTrks_train != 0]

        my_weights = [weights_train, trk_weights]

    elif args.model == "NMT":

        m = NMT(nHidden=args.nDecNodes, nDense=args.nFCNodes, doEmbedding=args.doEmbedding,
                timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                nFeatures=nFeatures, dataTag=dataTag, nClasses=args.nClasses,
                encUnits=args.nEncNodes, attnUnits=args.nAttnNodes)

        my_output = y_train_cat
        my_weights = weights_train

    elif args.model == "PFN":

        # For the deep sets, you don't need to specify the sort string
        dataTag = '{}_{}{}{}'.format(jetTag, varTag, ipTag, iterTag)

        ppm_sizes = strToList(args.ppm_sizes)
        dense_sizes = strToList(args.dense_sizes)

        m = myPFN(ppm_sizes=ppm_sizes, dense_sizes=dense_sizes, latent_dropout=args.latent_dropout,
                  timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                  nFeatures=nFeatures, dataTag=dataTag, nClasses=args.nClasses)

        my_output = y_train_cat
        my_weights = weights_train

    elif args.model == "DIPS":

        # For the deep sets, you don't need to specify the sort string
        dataTag = '{}_{}{}{}'.format(jetTag, varTag, ipTag, iterTag)

        ppm_sizes = strToList(args.ppm_sizes)
        dense_sizes = strToList(args.dense_sizes)

        m = DIPS(ppm_sizes=ppm_sizes, dense_sizes=dense_sizes, dropout=args.dropout,
                  timeSteps=timeSteps, loadModel=args.loadModel, modelDir=modelDir,
                  nFeatures=nFeatures, dataTag=dataTag, nClasses=args.nClasses,
                  batch_norm=args.batch_norm, weightFile=args.weightFile,modelTag=args.modelTag,attn=args.attention)

        my_output = y_train_cat
        my_weights = weights_train

    else:
        print("Error: {} is not a valid model".format(args.model))
        sys.exit()

    f.close()
    if args.model != "PFN":
        print(m.model.summary())

    '''
    Step 3: Train the corresponding model
    '''
    start = time.time()
    m.train(X_train, my_output, my_weights, nEpochs=args.nEpoch, X_jet_train=X_jet_train)
    end = time.time()

    print("Successfully trained model!")
    print("Training time: {:.0f} min".format((end-start)/60))
    os.system("nvidia-smi -L")
