# Create a script that automatically writes these bash submission files

import numpy as np
import os

def writeSubFile(pythonCmd, jobName):
    '''
    Write a job file to submit a python comand with a specified jobName
    '''
    # Open the file
    f = open('gpuSub_tmp.job', 'w')
    f.write("#!/bin/bash -l\n")

    options = {
        'P': 'trainNet.py',
        'n': 3,
        'W': '80:00',
        'q': 'slacgpu',
        'gpu': '"num=1:mode=exclusive_process:j_exclusive=no:mps=no"',
        'R': '"span[hosts=1]"'
        }

    # Specify the job name, and input and output logfiles
    fout = "output/{}.log".format(jobName)
    options['J'] = jobName
    options['e'] = fout 
    options['o'] = fout 

    if os.path.exists(fout):
        os.system("rm "+fout)

    for k,v in options.items():
        f.write("#BSUB -{} {}\n".format(k,v))

    # This command spins up the singularity image to allow the training to use the gpus
    singularitySetup = "singularity exec --nv -B /gpfs /gpfs/slac/atlas/fs1/d/nhartman/py3_pytorch1.simg "
    f.write(singularitySetup+pythonCmd)

    # Close the file
    f.close()

def timingExperiment(model,attention,nIters=5,iter=-1):
    '''
    Do the timing experiment study for section 4.1
    '''

    # Build up the python cmd
    pythonCmd =  "python3 trainNet.py --mc mc16d --jetCollection PFlow --nJets 3000000"
    pythonCmd += f" --mode train --model {model} --physicsSample ttbar --nTrks 15 --nClasses 3 "
    pythonCmd += f" --batch_norm --sortFlag sd0_rev --trkSelection ip3d --nLSTMNodes 100"
    
    if attention:
        pythonCmd += " --attention"
    attnTag = "attn" if attention else ""

    if iter == -1:
        
        for i in range(nIters):
            
            # Write the gpu submission file 
            writeSubFile(pythonCmd+f" --iter {i}",f"sec4.1_{model}{attnTag}_iter{i}")
                
            # Submit the job
            os.system("bsub < gpuSub_tmp.job")

    else:
    
        pythonCmd += f" --iter {iter}"
        
        writeSubFile(pythonCmd,f"sec4.1_{model}{attnTag}_iter{iter}")
        os.system("bsub < gpuSub_tmp.job")
        

def trkOpt():
    '''
    Train the network where I loosened the track selection to the JF cuts.
    '''
    
    # Build up the python cmd
    pythonCmd =  "python3 trainNet.py --mc mc16d --jetCollection PFlow --nJets 3000000"
    pythonCmd += f" --mode train --model DIPS --physicsSample ttbar --nTrks 40 --nClasses 3 "
    pythonCmd += f" --batch_norm --sortFlag sd0_rev --trkSelection jf  --attention"

    writeSubFile(pythonCmd,f"sec4.3.1_DIPSattn")
    os.system("bsub < gpuSub_tmp.job")
    

def newVars():
    '''
    '''
    pass


if __name__ == '__main__':

    from argparse import ArgumentParser

    '''
    Load in the options from the command line
    '''
    p = ArgumentParser()

    p.add_argument('--experiment', type=str, default='timing', 
                   help="Experiment to submit trainings for: timing (default), trkOpt")
    p.add_argument('--model', type=str, default='LSTM', dest="model",
                   help="Type of model: LSTM, LSTM_trkClass, NMT, PFN, DIPS")

    p.add_argument('--attention', action="store_true",
                   help='Whether to use an attention mechanism in the pooling operation (only supported for DIPS)')

    p.add_argument('--iter', type=int, default=-1, 
                   help='Iteration to run over for the timing experiment')
    p.add_argument('--nIters', type=int, default=5, 
                   help='# of iterations to train models for the timing experiments')

    args = p.parse_args()

    experiment = args.experiment
    model, attention = args.model, args.attention

    if experiment == 'timing':
        print(f"Running timing experiment {model}"+('attn' if attention else ''))
        timingExperiment(model,attention,args.nIters,args.iter)

    elif experiment == 'trkOpt':
        print("Training with JF cuts")
        trkOpt()

    else:
        print(f"Error: experiment {args.experiment} is not implemented... returning")

