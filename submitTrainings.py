
'''

submitTrainings.py

Goal: Write a submission script that can submit the (PFlow) networks that we
want for the retraining campaign studies!

'''

import os
import sys
from submitPreProcessing import submitJob
mc = 'mc16d'
jetCollection = 'PFlow'
mode = 'train'
nJets = int(3e6)

# my image (from Yee)
#img = "/afs/slac/package/singularity/images/slac-ml/20181002.0/slac-ml\@20181002.0.simg"
img = "/afs/slac/package/singularity/images/slac-ml/20181002.0/slac-ml@20181002.0.simg"
os.system('ls {}'.format(img))

job_name = ''
job_options = {
    'P':'trainNet.py',
    'n': 3,
    'W': '80:00',
    'q': 'slacgpu',
    'gpu': '"num=1:mode=exclusive_process:j_exclusive=no:mps=no"',
    'R': "span[hosts=1]",
    'B': "",
}

for physicsSample in ['ttbar','hybrid_5TeV']: # 'hybrid_1.5TeV'

    # Create the name for the input file
    job_name = '{}_{}_{}'.format(mc,jetCollection,physicsSample.replace(".","p"))

    job_options['J'] = job_name
    job_options['e'] = "output/{}-%J.log".format(job_name)
    job_options['o'] = "output/{}-%J.log".format(job_name)

    # Open the input file
    fname = "gpuSub_tmp.job"

    netParams = (mc,jetCollection,physicsSample, nJets, mode)
    cmd = 'singularity exec --nv -B /gpfs {} '.format(img)
    cmd += 'python trainNet.py  --mc {} --jetCollection {} --physicsSample {} --nJets {} --mode {}\n'.format(*netParams)

    # Write the training file

    # Submit the job

    submitJob(fname,job_options,cmd)
