from math import log10, floor
import numpy as np

def round_to_1(x):
    return round(x, -int(floor(log10(abs(x)))))

def nJetsTag(nJets):
    '''
    Make a tag that specifies the number of jets you have
    in the data sample, i.e, 5k for 5000.

    Input:
    Njets: the number of jets in the sample

    Output:
    jetTag (string): The number of jet in one sig fig with the
        units appended.
    '''

    if nJets < 1e3:
        jetTag = "{:.0f}".format(round_to_1(nJets))

    elif nJets < 1e6:
        # Appeding "k" to the file name, round to 1 sig fig
        jetTag = "{:.0f}k".format(round_to_1(nJets / 1e3))

    elif nJets < 1e9:
        jetTag = "{:.0f}m".format(round_to_1(nJets / 1e6))
    elif nJets < 1e12:
        jetTag = "{:.0f}g".format(round_to_1(nJets / 1e9))
    else:
        print('Note; you\'re running with more than a trillion events, are you sure this is what you want to do?')
        jetTag = "{:.0f}t".format(round_to_1(nJets / 1e12))

    return(jetTag)


def getIPTag(trkIPSgn):
    '''
    Given the tag you can feed into the argParser for the IP selection, return
    a tag to append to the datafile.

    Input:
    - trkIPSgn (str): Input from argParser

    Output:
    - ipTag (str): tag to append to a filename
     '''

    if trkIPSgn != "all":
        if trkIPSgn == "pos" or trkIPSgn == "positive":
            ipTag = "_posIP"
        elif trkIPSgn == "neg" or trkIPSgn == "negative":
            ipTag = "_negIP"
        else:
            print("ERROR: {} not a valid flag for trkIPSgn. Returning".format(trkIPSgn))
            return
    else:
        ipTag = ""

    return ipTag


def strToList(myStr, delimitter=","):
    '''
    Go from a string of a list of inputs to a list of strings

    Input:
    - myStr: Comma separated list of vars (in trk_xr)
    - delimitter: The delimmiter to split the list by
    Output:
    - myList: Comma separated list of strs

    '''

    if myStr == '-1':
        myList = []
    else:
        myList = myStr.replace(" ","").split(delimitter)

    # To standardize the order of the vars, first take the set of the list
    # you're returning
    #return list(set(myList))
    return myList

def getTrkMask(X, mask_value=0):
    '''
    Given an array for the track inputs, return a mask for the valid tracks.

    Inputs:
    - X: np array of shape (nJets, nTrks, nTrkFeatures)
    - mask_value: value corresponding to masked inputs (default 0)

    Output:
    - trk_mask: mask for valid tracks, of shape (nJets, nTrks)

    '''

    trk_mask = (np.sum(X, axis=-1) != mask_value)
    return trk_mask


def flatten(column):
    '''
    Args:
    -----
        column: a column of a pandas df whose entries are lists (or regular entries -- in which case nothing is done)
                e.g.: my_df['some_variable']

    Returns:
    --------
        flattened out version of the column.

        For example, it will turn:
        [1791, 2719, 1891]
        [1717, 1, 0, 171, 9181, 537, 12]
        [82, 11]
        ...
        into:
        1791, 2719, 1891, 1717, 1, 0, 171, 9181, 537, 12, 82, 11, ...

    Note: This function was taken from Micky's b-tagging ML tutorial
    http://nbviewer.jupyter.org/github/mickypaganini/btagging/blob/050bf033906b38ee8225e756ce5897a2236d5a88/data_handling_tutorial/tutorial.ipynb#

    '''
    try:
        return np.array([v for e in column for v in e])
    except (TypeError, ValueError):
        return column


def VR(pt,rho=30,Rmin=0.02,Rmax=0.4):
    '''
    The alg defining the radius of the jet

    Inputs:
    - pt: pT in [GeV]
    - rho, Rmin, Rmax: The parameters defining the instance of the VR alg,
                       default values correspond to ATLAS's implementation.
    '''

    R = rho / pt

	# Logic for is pt is passed as a float
    if R.size == 1:
        if R < Rmin: return Rmin
        if R > Rmin: return Rmax

    R[R < Rmin] = Rmin
    R[R > Rmax] = Rmax

    return R

def ftag_cone(pt):
    '''
    - pt in [GeV]
    '''
    pt_mev = pt * 1e3
    return 0.239 + np.exp(-1.22 - 1.64e-5 * pt_mev) #* pt_mev
