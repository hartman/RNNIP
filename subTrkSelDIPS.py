# Create a script that automatically writes these bash submission files

import numpy as np
import os

def writeSubFile(pythonCmd, jobName):
    '''
    Write a job file to submit a python comand with a specified jobName
    '''
    # Open the file
    f = open('gpuSub_tmp.job', 'w')
    f.write("#!/bin/bash -l\n")

    options = {
        'P': 'trainNet.py',
        'n': 3,
        'W': '80:00',
        'q': 'slacgpu',
        'gpu': '"num=1:mode=exclusive_process:j_exclusive=no:mps=no"',
        'R': '"span[hosts=1]"'
        }

    # Specify the job name, and input and output logfiles
    options['J'] = jobName
    options['e'] = "output/dips-{}-%J.log".format(jobName)
    options['o'] = "output/dips-{}-%J.log".format(jobName)

    for k,v in options.items():
        f.write("#BSUB -{} {}\n".format(k,v))

    # This command spins up the singularity image to allow the training to use the gpus
    singularitySetup = "singularity exec --nv -B /gpfs /gpfs/slac/atlas/fs1/d/nhartman/py3_pytorch1.simg "
    f.write(singularitySetup+pythonCmd)

    # Close the file
    f.close()


for pt_cut in [1.0]: #np.linspace(0.5, 1.0, 3):
    for d0_cut in [1.5,2.5,3.5]: #np.linspace(1.0, 3.5, 6):
        for z0_cut in [1.5,3.0,5.0]:

            trkTag = "pt_{}_d0_{}_z0_{}".format(pt_cut,d0_cut,z0_cut)
            pythonCmd = "python3 trainNet.py --mc mc16d --jetCollection PFlow --nJets 3000000 --mode train --model DIPS --physicsSample ttbar --nTrks 40 --batch_norm --sortFlag abs_sd0_rev --trkSelection "+trkTag

            print(trkTag)

            # Write the training file
            writeSubFile(pythonCmd,trkTag)
            # Submit the job
            os.system("bsub < gpuSub_tmp.job")
            #break
        #break
    #break
