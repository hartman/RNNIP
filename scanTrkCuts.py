'''
We've been learning this year that the RNN is more performant for higher efficiency
track selection than high purity, so one of the follow-ups R&D items that I wanted
to look into DIPS was see what gains in performance we could see if we reoptimized
the track selection.

To avoid overloading the main function of root_to_np.py too much, this script is
separate, but uses the same functions from root_to_np.py

Author: Nicole Hartman
July 2019
'''

import numpy as np
import pandas as pd
import h5py

import glob
import os
import sys
import tqdm

from root_to_np import *

from skhep.math.vectors import LorentzVector, Vector3D
import uproot
import xarray as xr

from usefulFcts import nJetsTag, getIPTag, strToList, VR

varTag = "sd0_sz0_nNextToInnHits_nInnHits_nsharedBLHits_nsplitBLHits_nsharedPixHits_nsplitPixHits_nsharedSCTHits_logNorm_ptfrac_dr_norm_nPixHits_nSCTHits"

if __name__ == '__main__':

	from argparse import ArgumentParser

	p = ArgumentParser()

	# p.add_argument('--batchSub', action="store_true",
	# 			   help='Submit the individual preprocessing steps to atlas-t3')

	p.add_argument('--filename', type=str,default="",help="Path to and name of .h5 files")

	p.add_argument('--nJets', type=int, default=int(3e6), dest="nJets",
	               help="Number of jets to process (default 3m)")

	p.add_argument('--pt',type=float,help='Track p_T cut [GeV]', default=0)
	p.add_argument('--d0',type=float,help='Track d0 cut [mm]', default=0)
	p.add_argument('--z0',type=float,help='Track z0 * sin(theta) cut [mm]', default=0)

	p.add_argument('--noNormVars', type=str, default='sd0,sz0,nNextToInnHits,nInnHits,'\
	               +'nsharedBLHits,nsplitBLHits,nsharedPixHits,nsplitPixHits,nsharedSCTHits',
	               help='Variables not to normalize: Pass -1 for an empty list')
	p.add_argument('--logNormVars', type=str, default='ptfrac, dr',
	               help='Variables to take the log of before normalizing, '\
	               +'default ptfrac, dr: Pass -1 for an empty list')
	p.add_argument('--jointNormVars', type=str, default='nPixHits,nSCTHits',
	               help='Variables to whiten: Pass -1 for an empty list')

	p.add_argument('--mode', type=str, default='',help='Mode for processing the data: \n'\
	               +'  default (''): process this many jets, and perform the train/test split\n'\
	               +'  train: process only the training jets, do the pT reweighting and scaling on all jets\n'\
	               +'  test: process only the test jets, loading in the scalingfile from the scalingfile arg\n')

	p.add_argument('--scalingfile', type=str, default='',help='String for loading in the scaling tag'
	               +' for running in test mode. If this is an empty string and you\'re running in'
	               +' the testing mode, it assumes that the same mc, jetCollection, and physicsSample'
	               +' should be used for the scaling and will recreate the appropriate scaling name to'
	               +' open the relevant file')

	p.add_argument('--scale_tag', type=str,default='',help='tag to append to the test file when a different'
	              +'scaling configuration was used.')

	args = p.parse_args()

	# Load in the command line arguments
	nJets, mode = int(args.nJets), args.mode
	pt, d0, z0 = args.pt, args.d0, args.z0

	# If the default values for the cuts are passed, then scan over the possible options
	pt_cuts = np.linspace(0.5, 1.0, 3) if pt == 0 else [ pt ]
	d0_cuts = np.linspace(1.0, 3.5, 6) if d0 == 0 else [ d0 ]
	z0_cuts = np.linspace(1.5, 5.0, 8) if z0 == 0 else [ z0 ]

	'''
	Step 1: Load in the data

	For this study, I'm going to use even file numbers for the training dataset
	and odd file numbers fo the test dataset.

	'''
	if len(args.filename) == 0:
		baseName = "../public/btag_hdf5/jf_cuts/user.hartman.410470.btagTraining.e6337_s3126_r10201_p3703.EMPFlow_IPRNN.2019-07-08-T095003-R18394_output_jfCuts.h5/user.hartman.18561042._000*[{}].output_jfCuts.h5"
		if mode == 'train':
			filename = baseName.format('02468')
		elif mode == 'test':
			filename = baseName.format('13579')
		else:
			filename = baseName.format('0123456789')
	else:
		filename = args.filename

	nTrks = 40
	jetTag = "{}{}_{}trks".format(nJetsTag(nJets),mode,nTrks)
	baseSubDir = "mc16d_PFlow_ttbar_"
	jet_df, trk_xr  = loadData(filename, nJets, baseSubDir+'jf', jetTag, nTrks)

	# Get the flattened pT dist
	if mode != 'test':
		pTReweight(jet_df)

	tpts = trk_xr.loc[:,:,'pt'].values
	td0s = trk_xr.loc[:,:,'IP3D_signed_d0'].values
	tz0s = trk_xr.loc[:,:,'IP3D_signed_z0'].values

	'''
	Step 2: Loop over the track selections and create the track masks
	'''
	for pt_cut in pt_cuts:
		for d0_cut in d0_cuts:
			for z0_cut in z0_cuts:

				print("pt: {}, d0: {}, z0: {}".format(pt_cut, d0_cut,z0_cut))

				# Get the track mask, and save the df and trk xarrays
				mask = (tpts > pt_cut) & (np.abs(td0s) < d0_cut) & (np.abs(tz0s) < z0_cut)

				'''
				Step 3: Prepare for keras
				'''
				subDir = baseSubDir+"pt_{}_d0_{}_z0_{}".format(pt_cut,d0_cut,z0_cut)
				if not os.path.exists("data/"+subDir):
					os.mkdir("data/"+subDir)
					os.mkdir("models/"+subDir)
					os.mkdir("figures/"+subDir)

				if len(args.scalingfile) == 0:
					scalingfile = "data/{}/scale_3mtrain_{}trks_{}.json".format(subDir,nTrks,varTag)
				else:
					scalingfile = args.scalingfile

				print(scalingfile)

				prepareForKeras(jet_df, trk_xr, jetTag, args.noNormVars,
								args.logNormVars, args.jointNormVars, jetVars=[],mode=mode,
								scalingfile=scalingfile, scale_tag=args.scale_tag,
								subDir=subDir, sortFlag="abs_sd0_rev", trkMask=mask)
