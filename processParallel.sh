#!/bin/bash -u

# ttbar 
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample ttbar --mode test
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample ttbar --mode train

# Z' 
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample Zprime_1.5TeV --mode test
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample Zprime_1.5TeV --mode train

# extended Z'
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample Zprime_5TeV --mode test
python parallelPreprocessing.py --nTrks 25 --mc mc16d --jetCollection PFlow --physicsSample Zprime_5TeV --mode train
