'''
parallelPreprocessing.py

Write a script that can run in two separate modes:
1. Run over a single one of Binbin's files to produce output dfs and xarrays
2. Loop over all a set of files in a directory and submit them to the LSF batch to
   preprocess them with uproot

Since this function is a wrapper around my getData and saveData functions in root_to_np.py,
I'm going to use most of the same arguments that the arg parser uses for that script.

I'm modelling the logic of this function off of Rafael's root_to_hdf5.py script.
https://gitlab.cern.ch/rateixei/munn/blob/master/data_processing/root_to_hdf5.py

'''

from root_to_np import *
import re
import glob
from root_to_np import getData, saveData

NtupleDir = '/u/ki/nhartman/gpfs/public/'

Ntuples = {

    'mc16a_PFlow_ttbar' : 'user.bdong.410470.ttbar_hdamp258p75_nonallhad.mc16a_newDerivation_noRetag_Akt4EMPf/user.bdong.16267610.Akt4EMPf._00*.root',
    'mc16a_PFlow_Zprime_1.5TeV' : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16a_newDerivation_noRetag_Akt4EMPf/user.bdong.16228899.Akt4EMPf._000*.root',

    'mc16d_PFlow_ttbar'         : 'user.bdong.410470.ttbar_hdamp258p75_nonallhad.mc16d_newDerivation_noRetag_Akt4EMPf/user.bdong.16276417.Akt4EMPf._00*.root',
    'mc16d_PFlow_Zprime_1.5TeV' : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16d_newDerivation_noRetag_rerun_Akt4EMPf/user.bdong.16288899.Akt4EMPf._000*.root',
    'mc16d_PFlow_Zprime_5TeV'   : 'user.bdong.mc16_13TeV.427081.flatpT_Zprime_Extended.mc16d_newDerivation_noRetag_Akt4EMPf/user.bdong.16288071.Akt4EMPf._00*.root',
    'mc16d_VR_ttbar'   : 'user.bdong.410470.ttbar_hdamp258p75_nonallhad.mc16d_newDerivation_noRetag_AktVR30Rmax4Rmin02Tr/user.bdong.16276417.AktVR30Rmax4Rmin02Tr._000*.root',
    'mc16d_VR_Zprime_1.5TeV'   : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16d_newDerivation_noRetag_rerun_AktVR30Rmax4Rmin02Tr/user.bdong.16288899.AktVR30Rmax4Rmin02Tr._000*.root',
    'mc16d_VR_Zprime_5TeV'     : 'user.bdong.mc16_13TeV.427081.flatpT_Zprime_Extended.mc16d_noRetag_Apr29_AktVR30Rmax4Rmin02Tr/user.bdong.17895253.AktVR30Rmax4Rmin02Tr._000*.root',
    'mc16d_VR_hybrid_1.5TeV_cut_125GeV'   : 'FrancescoNtuples/mc16d_VR_hybrid_1.5TeV/VR_Hybrid_10_4_2019_1.root',
    'mc16d_VR_hybrid_5TeV_cut_125GeV'   : 'FrancescoNtuples/mc16d_VR_hybrid_5TeV/VR_Extended_RNNSMT_060519.root',

    # Samples from PF for the "perfect tracking" studies
    'mu40_Topo_Zprime_5TeV_PT' : 'PF_Ntuples/user.pbutti.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.FlavourTagPerformanceTools_PT_v01_Akt4EMTo/user.pbutti.17774615.Akt4EMTo._0000*.root',
    'mu40_Topo_Zprime_5TeV_NT' : 'PF_Ntuples/user.pbutti.427081.Pythia8EvtGen_A14NNPDF23LO_flatpT_Zprime_Extended.FlavourTagPerformanceTools_v02_Akt4EMTo/user.pbutti.17773333.Akt4EMTo._0000*.root',
    'mu40_Topo_Zprime_5TeV_sctSplit' : 'PF_Ntuples/user.pbutti.427081.flatpT_Zprime_Extended.FlavourTagPerformanceTools_SCTSplit_v01_Akt4EMTo/user.pbutti.18259868.Akt4EMTo._0000*.root',
    'mu40_Topo_Zprime_5TeV_Bcut' : 'FrancescoNtuples/btracking/user.fdibello.mc16_13TeV.427080_flatpT_ZprimeExte_Bcut_Akt4EMTo/user.fdibello.18225562.Akt4EMTo._0000*.root',
    'mu40_Topo_Zprime_5TeV_BcutRefit' : 'FrancescoNtuples/btracking/user.fdibello.mc16_13TeV.427080_flatpT_ZprimeExte_BcutAll_Akt4EMTo/user.fdibello.18266251.Akt4EMTo._0000*.root'
}


def run_one(filename,jetCollection='PFlow',sort_flag='sd0_rev',trkSelection='',
			nTrks=15,subDir='',sortFlag='sd0_rev',mode='test',skipEvt=0,
            min_evt=-1,max_evt=-1,tag='',jetsPerEvt=-1):
    '''
    Run over a single file

	Inputs:
	- filename: Directory to and name of the root file
    - jetCollection: the jet collection to use - Topo (default) or PFlow
    - sort_flag: The variable specifying what we're sorting by
    - trkSelection: A string indicating which tracks to select. If it's an empty
                    string, use the ip3d tracks, otherwise the string specifies
                    the pt, d0, and z0SinTheta cuts to use.
    - nTrks: The number of tracks to truncate / pad the sequences to
    - subDir: Folder inside data/ to save the output to
    - sortFlag: The flag to append to the datafile to represent the sorting
                 scheme used
    - mode: Str to append to filename for whether you're processing train, test, or all ('') the data
    - skipEvt: The events where evtNum % 2 == skipEvt is the event to skip
	- tag: If non-empty, a tag to append to the names of the jet df and trk xrs
    - jetsPerEvt: Takes up to many jets in the event, assumed to be leading in pT

    '''


	# Below are the jet and trk variables for accessing the events out of the root file
    jet_vars = ["jet_LabDr_HadF","jet_pt_orig","jet_eta_orig","jet_phi_orig","jet_m",
                #"jet_ip3d_llr","jet_ip3d_pb","jet_ip3d_pc","jet_ip3d_pu",
                "jet_ip3d_ntrk",
                "jet_aliveAfterOR","jet_aliveAfterORmu",
                "jet_JVT","jet_pt","jet_eta", "jet_phi",
                "jet_nBHadr","jet_nCHadr"]

    bhadron_vars = ["jet_bH_pt","jet_bH_eta","jet_bH_Lxy","jet_bH_dRjet",
                    "jet_bH_PtTrk","jet_bH_MTrk","jet_bH_nBtracks",
                    "jet_bH_nCtracks","jet_bH_nBtracks_400","jet_bH_nCtracks_400"]

    derived_jet_vars = ["vecSumTrkPt"]

    trk_vars  = ["jet_trk_ip3d_d0sig", "jet_trk_ip3d_z0sig",
				"jet_trk_ip3d_d0", "jet_trk_ip3d_z0",
                "jet_trk_ip3d_grade",
                "jet_trk_pt", "jet_trk_eta", "jet_trk_theta", "jet_trk_phi",
                "jet_trk_orig",
                "jet_trk_nInnHits","jet_trk_nsharedBLHits","jet_trk_nsplitBLHits",
                "jet_trk_nNextToInnHits",
                "jet_trk_nPixHits","jet_trk_nPixHoles",
                "jet_trk_nsharedPixHits","jet_trk_nsplitPixHits",
                "jet_trk_nSCTHits","jet_trk_nSCTHoles",
                "jet_trk_nsharedSCTHits",
                ]
    evt_vars = ['avgmu','eventnb']
    derived_trk_vars = ['jet_trk_ptfrac','jet_trk_dr',
                       'jet_trk_prbP','jet_trk_pPerp']

    if jetCollection == "VR": jet_vars += ["jet_nConst"]

	# Chooose nJets to be something much larger than the # of jets that would be in a single
    # Ntuple so that you process all of the jets in the file
    nJets = int(5e6)

    jet_list, trk_list, jetVars, trkVars  = getData(filename, nJets, jetCollection, sortFlag,
                                                    jet_vars, bhadron_vars, derived_jet_vars, evt_vars, trk_vars, derived_trk_vars,
                                                    skipEvt=skipEvt, min_evt=min_evt, max_evt=max_evt,jetsPerEvt=jetsPerEvt)

    print(jetVars)

    jet_df, trk_xr  = saveData(jet_list, trk_list, jetVars, trkVars, nTrks, subDir,
                               sortFlag, mode, tag=tag)


def getFileTag(physicsSample, filename, min_evt, max_evt):
    '''
    Access the file name corresponding to which parameters we're using in the
    file split.

    If we're looking at a hybrid file, we're going to be splitting the file by
    entries, whereas if it's a ttbar or Z' file, we're going to just use the
    file splits that Binbin had when producing the Ntuples, so index by the
    filenumber.

    Inputs:
    - physicsSample: The physics sample we're running over
    - filename: The name of the current file
    - min_evt, max_evt: The min and max entries for the event

    Outputs:
    - fileTag: A tag to append to the output file and the job names we're submitting

    '''

    if 'hybrid' in physicsSample:

        fileTag = '_entry_{}_to_{}'.format(min_evt, max_evt)

    else:
        fileNum = int(re.findall(r'\d+', filename)[-1])
        fileTag = '_{:06d}'.format(fileNum)

    return fileTag

def getTreeName(jetCollection):
    '''
    Return the tree name for a given jet collection
    '''


    if args.jetCollection == "VR":
        jetCollectionName = "VR30Rmax4Rmin02Track"
    else:
        jetCollectionName = "4EM" + args.jetCollection
    treeName = "bTag_AntiKt{}Jets".format(jetCollectionName)
    return treeName


if __name__ == '__main__':

	from argparse import ArgumentParser

	p = ArgumentParser()
	p.add_argument('--nTrks', type=int, default=15, dest="nTrks",
	               help="Maximum number of tracks (default 15)")

	p.add_argument('--filename', type=str, default="",
				   dest="filename", help="If empty (default) access the files from Binbin using the mc, jetCollection, and physicsSample tags,"\
				   +"submitting the jobs in parallelto the LSF batch queue."
				   +"If non-empty, will run over the file(s) in the interactive mode.")

	p.add_argument('--min_evt', type=int, default=-1,
				   dest="min_evt", help="Load in the ttree starting w/ this entry: only works if filename is passed")
	p.add_argument('--max_evt', type=int, default=-1,
				   dest="max_evt", help="Load in the ttree stopping w/ this entry: only works if filename is passed")

	# The info for which files to run over
	p.add_argument("--mc", type=str, default="mc16d", dest="mc",
	               help="mc version: mc16a, mc16d (default), mc16e")
	p.add_argument('--jetCollection', type=str, default='PFlow', dest="jetCollection",
	               help="Jet collection to train over: Topo, PFlow (default), VR")
	p.add_argument('--physicsSample', type=str, default='ttbar', dest="physicsSample",
	               help="the physics sample of interest: can be one of ttbar (default),"
	               +"Zprime_1.5TeV, Zprime_5TeV")

	p.add_argument('--sortFlag', type=str, dest="sortFlag", default="sd0_rev",
	               help="Sorting configuration for the tracks, default sd0_rev.")

	p.add_argument('--trkSelection',type=str,dest="trkSelection",default='',
	               help="The track selection to use: just a tag which can get appended"\
                   +" to the file name '' (default), PT, NT, sctSplit, Bcut")

	p.add_argument('--mode', type=str, default='test',help='Mode for processing the data: \n'\
	               +'  train: only process even events\n'\
	               +'  test (default): only process odd events\n'\
	               +'  any other option: process all events\n')

	args = p.parse_args()

	# The subDir in data/ to save the dfs + xrs to
	physKey = args.physicsSample if '2lead' not in args.physicsSample else args.physicsSample[:-6]
	subDir = "{}_{}_{}{}/files_{}trks_{}".format(args.mc, args.jetCollection, args.physicsSample,
	                                             "_"+args.trkSelection if len(args.trkSelection) != 0 else "",
	                                             args.nTrks, args.sortFlag)

	# If you're running with a filename passed, just run getData() and saveData for all of the jets in the file
	if len(args.filename) > 0:

		# Select the events of interest based on the mode you're running in
		if args.mode == 'train' and 'hybrid' not in args.physicsSample:
			print('Running in the train mode: only keep even event numbers')
			skipEvt = 1
		elif args.mode == 'test' and 'hybrid' not in args.physicsSample:
			print('Running in the test mode: only keep odd event numbers')
			skipEvt = 0
		else:
			if 'hybrid' in args.physicsSample:
				print('Just process all of the events from Francesco\'s file.')
			else:
				print('Do the train / test split and keep all the events')
			skipEvt = 2


		fileTag = getFileTag(args.physicsSample, args.filename, args.min_evt, args.max_evt)

		# Check if we only want to save a specified # of jets / evt (assumed to be leading in pT)
		jetsPerEvt = 2 if '2lead' in args.physicsSample else -1

		print("min_evt = {}, max_evt = {}".format(args.min_evt,args.max_evt))
		run_one(args.filename, jetCollection=args.jetCollection, #trkSelection=args.trkSelection,
				nTrks=args.nTrks,subDir=subDir,sort_flag=args.sortFlag,
				mode=args.mode,skipEvt=skipEvt,
				min_evt=args.min_evt, max_evt=args.max_evt, tag=fileTag, jetsPerEvt=jetsPerEvt)


    # Otherwise, loop over all of the individual files and submit the jobs
	else:

		# Create the new directory if it doesn't exist
		myDir = 'data/{}'.format(subDir)
		if not os.path.exists(myDir):
			print('Creating new directory',myDir)
			os.makedirs(myDir)


		# Loop over all of the files created by Binbins for this mc, jetCollection, and physicsSample
		key = "{}_{}_{}".format(args.mc, args.jetCollection, physKey)
		if len(args.trkSelection) != 0:
			key += "_{}".format(args.trkSelection)
		print( NtupleDir + Ntuples[key] )
		for filename in glob.glob( NtupleDir + Ntuples[key] ):

			pythonCmd =  "python parallelPreprocessing.py --filename " + filename

			# Find the file number associated to this Ntuple
			# fileNum = int(re.findall(r'\d+', filename)[-1])

			# Append all of the other arg parser arguments except filename
			for k,v in vars(args).items():
				if (k == 'filename') or ('evt' in k) or (k == 'trkSelection' and len(v) == 0):
					continue
				pythonCmd += ' --{} {}'.format(k,v)

			if 'hybrid' in args.physicsSample:

				#  Open the file to see how many entries there are
				nentries = uproot.open(filename)[getTreeName(args.jetCollection)].numentries

				# Put in an extra loop to batchify the entries
				step = 5000
				for min_evt, max_evt in zip(range(0,nentries,step), range(step,nentries+1,step)):
					# Append new variables to the python command
					hybPythonCmd = "{} --min_evt {} --max_evt {}".format(pythonCmd,min_evt,max_evt)

					print(hybPythonCmd)

					fileTag = getFileTag(args.physicsSample, filename, min_evt, max_evt)
					job_name = '{}_{}{}'.format(key,args.mode,fileTag)
					bsubCmd = 'bsub -W 80:00 -q atlas-t3 -oo output/{}.txt -J {} {}'.format(job_name, job_name, hybPythonCmd)
					os.system(bsubCmd)


			else:

				print(pythonCmd)

				fileTag = getFileTag(args.physicsSample, filename, args.min_evt, args.max_evt)
				job_name = '{}_{}{}'.format(key,args.mode,fileTag)
				bsubCmd = 'bsub -W 80:00 -q atlas-t3 -oo output/{}.txt -J {} {}'.format(job_name, job_name, pythonCmd)
				os.system(bsubCmd)


			#break
