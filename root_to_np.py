'''
root_to_np.py

Go from root files to numpy arrays.
Apply any jet or track level cuts for the RNN studies.
Save Keras ready inputs for training.

Nicole Hartman, Fall 2018

'''

import numpy as np
import pandas as pd
from random import shuffle
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
import h5py

import glob
import os
import sys
import tqdm

from skhep.math.vectors import LorentzVector, Vector3D
import uproot
import xarray as xr

from usefulFcts import nJetsTag, getIPTag, strToList, VR

# Get the map from the notation of the hdf5 dumper from Manuel to the notation
# from the optimization framework
var_map = {
    'IP3D_signed_d0_significance' : 'sd0',
    'IP3D_signed_z0_significance' : 'sz0',
    'IP2D_signed_d0' : 'ip2d_d0',
    'IP3D_signed_d0' : 'ip3d_d0',
    'IP3D_signed_z0' : 'ip3d_z0',
    'btag_ip_d0' : 'd0',
    'btag_ip_z0' : 'z0',
    'numberOfNextToInnermostPixelLayerHits' : 'nNextToInnHits',
    'numberOfInnermostPixelLayerHits' : 'nInnHits',
    'numberOfInnermostPixelLayerSharedHits' : 'nsharedBLHits',
    'numberOfInnermostPixelLayerSplitHits' : 'nsplitBLHits',
    'numberOfPixelSharedHits' : 'nsharedPixHits',
    'numberOfPixelSplitHits' : 'nsplitPixHits',
    'numberOfSCTSharedHits' : 'nsharedSCTHits',
    'numberOfPixelHits' : 'nPixHits',
    'numberOfSCTHits' : 'nSCTHits',
    }

# Rescale the pTs from MeV -> GeV
MeVtoGeV = lambda x: x*0.001

def get_sort_index(arr, sort_type=None):
    '''
    Find the indices that sort an array

    Inputs:
    - arr: A 1d np.array that we're sorting by
    - sort_type: A string indicating what type of variables that we're sorting by
        - If 'abs' is in sort_type, it will use the abs of arr for the sort
        - If 'neg' is in sort_type, it will multiply the array by -1 for the sort
        - If 'rev' is in the string, the sort will be in descending order.

    Output:
    - indices: An np.array of ints of the same shape as arr

    '''

    if 'abs' in sort_type:
        indices = np.argsort( np.abs(arr) )
    elif 'neg' in sort_type:
        raise NotImplementedError
        #indices = np.argsort(-arr)
    else:
        indices = np.argsort(arr)

    if 'rev' in sort_type:
        return indices[::-1]
    else:
        return indices

def getdR(jeta, jphi, teta, tphi):
    '''
    Calculate dR, accounting for the 2pi difference b/w for phi b/w pi and -pi.

    Inputs:
    - jeta, jphi: (floats) for the jet axis direction
    - teta, tphi: (np.arrays) for the track dir

    Returns:
    - tdrs: An np array for the opening angles b/w the tracks and the jet axis.

    '''

    deta = teta - jeta

    # Calculate delta phi, accounting the for the 2pi at the boundary
    dphi = tphi - jphi
    dphi[dphi >  np.pi] = dphi[dphi >  np.pi] - 2*np.pi
    dphi[dphi < -np.pi] = dphi[dphi < -np.pi] + 2*np.pi

    tdrs = np.sqrt( deta**2 + dphi**2)
    return tdrs

def passJet(jet_arrays, trk_arrays, ievt, ijet, cut_jet_pt=20000, cut_JVT=0.59, cut_nConst=-1):
    '''
    Figure out whether a given jet passes the standard cuts.
    Returns True if the jet passes the standard cuts, and false
    otherwise.

    Input:
    - jet_arrays: Uproot arrays for the jet vars
    - trk_arrays: Uproot arrays for the trk vars
    - ievt: Event number
    - ijet: Jet number

    '''

    # The variables used for processing the array
    cut_jet_eta = 2.5

    # jet pt cut
    if jet_arrays[b"jet_pt"][ievt][ijet] < cut_jet_pt:
        return False

    # jet eta cut
    if np.abs(jet_arrays[b"jet_eta"][ievt][ijet]) > cut_jet_eta:
        return False

    # Electron overlap removal
    if not jet_arrays[b'jet_aliveAfterOR'][ievt][ijet]:
        return False

    # Muon overlap removal
    if not jet_arrays[b'jet_aliveAfterORmu'][ievt][ijet]:
        return False

    # JVT cut
    if cut_JVT != -1:
        if (jet_arrays[b'jet_pt'][ievt][ijet] < 60000) & \
            ((np.abs(jet_arrays[b'jet_eta'][ievt][ijet]) < 2.4)) & \
            (jet_arrays[b'jet_JVT'][ievt][ijet] < cut_JVT):
            return False

    # Number of constituents cut. This only applies for VR track jets, so if
    # this has it's non-default value, apply the jet overlap removal as well
    if cut_nConst != -1:

        if jet_arrays[b'jet_nConst'][ievt][ijet] < cut_nConst: return False

        # Find the vector of dRs between the current jet and all other jets in
        # the event with pT > 5 GeV and at least two tracks.
        # An eta cut on the VR track jets is trivial since you only form VR
        # track jets with tracks with |eta| < 2.5
        otherJetMask = (jet_arrays[b'jet_pt'][ievt] > 5000) & (jet_arrays[b'jet_nConst'][ievt] >= cut_nConst)

        # We don't want to calculate dr w/r.t. the current jet
        otherJetMask[ijet] = False

        # Calculate the radius of this jet given the pt
        Ri = VR(jet_arrays[b'jet_pt'][ievt][ijet])

        # Find the min radius between each track jet and the other track jet
        minJetRs = np.array(VR(jet_arrays[b'jet_pt'][ievt][otherJetMask] / 1000))
        minJetRs[Ri < minJetRs] = Ri

        # Get the dr w/ jet i with the other valid jets
        dr_jets = getdR(jet_arrays[b'jet_eta'][ievt][ijet],jet_arrays[b'jet_phi'][ievt][ijet],
                        jet_arrays[b'jet_eta'][ievt][otherJetMask],jet_arrays[b'jet_phi'][ievt][otherJetMask])

        # If you have a case where the opening angle between te jets is smaller
        # than one of jet radii, don't keep the jet
        if np.sum(dr_jets < minJetRs): return False

    if len(trk_arrays[b'jet_trk_ip3d_grade'][ievt][ijet]) == 0:
        return False

    return True

class trkCuts():
    '''
    Apply the pT and IP cuts to the tracks
    '''

    def __init__(self,tpt,d0,z0):
        self.tpt_cut = tpt
        self.td0_cut = d0
        self.tz0_cut = z0

    def getTrkMask(self,tarrays,ievt,ijet):
        '''
        Return the track mask for the given event and jet in tarrays
        '''
        return ( np.array(tarrays[b'jet_trk_pt'][ievt][ijet]) > self.tpt_cut) \
               & (np.abs(np.array(tarrays[b'jet_trk_ip3d_d0'][ievt][ijet])) < self.td0_cut) \
               & (np.abs(np.array(tarrays[b'jet_trk_ip3d_z0'][ievt][ijet])) < self.tz0_cut) \
               & (np.abs(np.array(tarrays[b'jet_trk_eta'][ievt][ijet])) < 2.5) \
               & (np.floor((np.array(tarrays[b'jet_trk_nsharedPixHits'][ievt][ijet]) 
                           +np.array(tarrays[b'jet_trk_nsharedSCTHits'][ievt][ijet])) / 2) <= 1) \
               & ((np.array(tarrays[b'jet_trk_nPixHits'][ievt][ijet]) \
                  +np.array(tarrays[b'jet_trk_ndeadPixSensors'][ievt][ijet]) \
                  +np.array(tarrays[b'jet_trk_nSCTHits'][ievt][ijet]) \
                  +np.array(tarrays[b'jet_trk_ndeadSCTSensors'][ievt][ijet])) >= 7) \
               & ((np.array(tarrays[b'jet_trk_nPixHoles'][ievt][ijet]) \
                  +np.array(tarrays[b'jet_trk_nSCTHoles'][ievt][ijet])) <= 2) \
               & (np.array(tarrays[b'jet_trk_nPixHoles'][ievt][ijet]) <= 1) 


def ip3d_mask(tarrays,ievt,ijet):
    '''
    Get the mask for the tracks in the event for the ip3d tracks
    '''
    return (np.array(tarrays[b'jet_trk_ip3d_grade'][ievt][ijet]) != -10)


def getData(filename="/gpfs/slac/atlas/fs1/d/rafaeltl/public/RNNIP/FTAG_ntups/"\
                     +"user.rateixei.mc16_13TeV.410470."\
                     +"PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD."\
                     +".dijetSamplesNominal20180629_Akt4EMTo/"\
                     +"user.rateixei.14594351.Akt4EMTo._000*.root",
            nJets=200000, jetCollection="Topo", timestamp='',sort_flag="sd0_rev",
            jet_vars=[], bhadron_vars=[], derived_jet_vars=[], evt_vars=[],
            trk_vars=[], derived_trk_vars=[],trkSelection="",
			skipEvt=2, min_evt=-1, max_evt=-1, jetsPerEvt=-1):
    '''
    Open the root files processed by Rafael, access the desired jet and trk
    variables, apply the jet level cuts, implement the desired track sort,
    and return the lists for the desired jet and track variables of
    length nJets.

    Jet Cuts:
        - Calibrated jet pT > 20 GeV, |eta| < 2.5
        - JVT > 0.59 (0.2) for central, low pT Topo (PFlow) jets
        - Jet can't overlap with a previously IDed photon or muon
    - Also rescale pTs from MeV -> GeV

    Inputs:
    - filename: Directory to and name of the root file
    - nJets: Max # of events (default 200k). If you request more jets than are there, then
             it should keep running and just return when we run out of jets.
    - jetCollection: the jet collection to use - Topo (default) or PFlow
    - sort_flag: The variable specifying what we're sorting by
    - trkSelection: A string indicating which tracks to select. If it's an empty
                    string, use the ip3d tracks, otherwise the string specifies
                    the pt, d0, and z0SinTheta cuts to use.
    - skipEvt: Skip the evts that have ievt % 2 == skipEvts
               To be consistent with the MV2 team:
			   * Set to 1 for training to keep EVEN numbered events
			   * Set to 0 for testing to keep ODD numbered events
			   * Set to 2 (default) for keeping all events

    - min_evt, max_evt: If these entries are not -1, then they allow you to
                            read in just a part of the TTree, which can be useful
                            for parallelizing the processing of large files.

    - jetsPerEvt: Takes up to many jets in the event, assumed to be leading in pT.
                  The default option of -1 keeps *all* of the jets in the event.


    Outputs:
    - output_jet_array: A list of the corresponding jet variables
    - output_trk_array: A list of lists for the trk variables
    - jet_vars: The variable names of the jet variables from the Ntuple
    - trk_vars: The variable names of the trk variables from the Ntuple

    '''


    print('Running over {} jets'.format(jetCollection))

    # Declare the output arrays that we're going to put stuff into?
    output_jet_array = []
    # Each entry here should be a list of lists to use Keras's pad_sequences function
    output_trk_array = {k:[] for k in trk_vars + derived_trk_vars}

    # Some constants needed in the TCT derived variables definitions
    trkMinPtCut = 700 # MeV at this point
    coeffPt = 10.
    mpi = 139.57 # pion mass (MeV)

    cut_JVT = 0.59 if jetCollection == 'Topo' else 0.2 if jetCollection == 'PFlow' else -1
    cut_pT = 10000 if jetCollection == "VR" else 20000
    cut_nConst = 2 if jetCollection == "VR" else -1

    #if len(trkSelection) > 0:
    if 'pt' in trkSelection and 'd0' in trkSelection and 'z0' in trkSelection:
        tCuts = trkSelection.split("_")
        tpt_cut = float(tCuts[1]) # MeV
        td0_cut = float(tCuts[3]) # mm
        tz0sinTheta_cut = float(tCuts[5]) # mm

        t = trkCuts(tpt_cut, td0_cut, tz0sinTheta_cut)
        f = t.getTrkMask

    else:
        f = ip3d_mask

    # Determine the sort_var from the sort_flag
    if "sd0" in sort_flag:
        sort_var = b"jet_trk_ip3d_d0sig"
    elif "sz0" in sort_flag:
        sort_var = b"jet_trk_ip3d_z0sig"
    elif "pt" in sort_flag:
        sort_var = b"jet_trk_pt"
    else:
        print("Error: No supported sorting variable for sort_flag `{}` in getData().".format(sort_flag))
        return

    print("Sorting by {}".format(sort_var))

    # Open the file and get the np arrays
    for fname in glob.glob( filename ):

        print("Opening file {}".format(fname))
        print("min_evt = {}, max_evt = {}".format(min_evt,max_evt))

        # Access the ttree
        if jetCollection == "VR":
            jetCollectionName = "VR30Rmax4Rmin02Track"
        else:
            jetCollectionName = "4EM" + jetCollection
        treeName = "bTag_AntiKt{}Jets{}".format(jetCollectionName,timestamp)

        if min_evt < max_evt and min_evt > -1:

            print("Only loading in entries {} - {}".format(min_evt,max_evt))

            evt_arrays = next(uproot.iterate(fname, treeName,
                                              branches=evt_vars,
                                              entrysteps=[(min_evt,max_evt)] ))
            jet_arrays = next(uproot.iterate(fname, treeName,
                                              branches=jet_vars,
                                              entrysteps=[(min_evt,max_evt)] ))
            trk_arrays = next(uproot.iterate(fname, treeName,
                                              branches=trk_vars,
                                              entrysteps=[(min_evt,max_evt)] ))

        else:

            myTree = uproot.open(fname)[treeName]

            # Select the jet and trk arrays
            evt_arrays = myTree.arrays(evt_vars)
            jet_arrays = myTree.arrays(jet_vars+bhadron_vars)
            trk_arrays = myTree.arrays(trk_vars)

        ## Event Loop
        n_events = len(jet_arrays[b'jet_pt'])
        print('n_events',n_events)

        for ievt in range(n_events):

            if ievt % 2 == skipEvt:
                continue

            if len(output_jet_array) % 10000 == 0:
                print('    Jet {} / {}'.format(len(output_jet_array), int(nJets)))

            # Jet loop
            jetCounter = 0
            n_jets = len(jet_arrays[b"jet_pt"][ievt])
            for ijet in range(n_jets):

                if not passJet(jet_arrays, trk_arrays, ievt, ijet, cut_pT, cut_JVT, cut_nConst):
                    continue

                jetCounter += 1

                # Append to a list for variables of the jet class
                # The encode() function just lets us go from a string to as bytes literal
                # https://stackoverflow.com/questions/45360480/is-there-a-formatted-byte-string-literal-in-python-3-6

                jetList = [jet_arrays[jvar.encode()][ievt][ijet] for jvar in jet_vars if jvar != 'jet_bH_Lxy']

                # The jet_bH_* variables are lists of the Lxys for b-hadrons in the jet
                # (i.e, we have two b-hadrons for a double b-tagger), so we need
                # to extract these variable differently
                jetList = jetList + [jet_arrays[bvar.encode()][ievt][ijet][0] for bvar in bhadron_vars]

                evtList = [evt_arrays[evar.encode()][ievt] for evar in evt_vars]

                # Get the trk variable mask for this jet
                trk_mask = f(trk_arrays,ievt,ijet)
                #if len(trkSelection) == 0:
                #trk_mask = (np.array(trk_arrays[b'jet_trk_ip3d_grade'][ievt][ijet]) != -10)
                #else:
                #    trk_mask = (np.array(trk_arrays[b'jet_trk_pt'][ievt][ijet]) > tpt_cut) \
                #              & (np.abs(np.array(trk_arrays[b'jet_trk_ip3d_d0'][ievt][ijet])) < td0_cut) \
                #              & (np.abs(np.array(trk_arrays[b'jet_trk_ip3d_z0'][ievt][ijet])) < tz0sinTheta_cut)

                index_list = get_sort_index( np.array(trk_arrays[sort_var][ievt][ijet])[trk_mask],
                                             sort_type=sort_flag )

                for tvar in trk_vars:
                    # Do the IP3D track selection + sort
                    output_trk_array[tvar].append( np.array(trk_arrays[tvar.encode()][ievt][ijet])[trk_mask][index_list] )

                # Now calculate the dr and ptfrac variables
                dr = getdR(jet_arrays[b'jet_eta_orig'][ievt][ijet],
                           jet_arrays[b'jet_phi_orig'][ievt][ijet],
                           output_trk_array['jet_trk_eta'][-1],
                           output_trk_array['jet_trk_phi'][-1])

                ptFrac = output_trk_array['jet_trk_pt'][-1] / jet_arrays[b'jet_pt_orig'][ievt][ijet]

                output_trk_array['jet_trk_dr'].append(dr)
                output_trk_array['jet_trk_ptfrac'].append(ptFrac)

                # TCT variables: Add the prbP and p_perp variables that Vadim was using
                pfrac = (output_trk_array['jet_trk_pt'][-1] - trkMinPtCut) / np.sqrt(jet_arrays[b'jet_pt_orig'][ievt][ijet])
                prbP = pfrac / (coeffPt + pfrac)
                output_trk_array['jet_trk_prbP'].append(prbP)

                jet = LorentzVector()
                jet.setptetaphim(jet_arrays[b'jet_pt_orig'][ievt][ijet],
                                 jet_arrays[b'jet_eta_orig'][ievt][ijet],
                                 jet_arrays[b'jet_phi_orig'][ievt][ijet],
                                 jet_arrays[b'jet_m'][ievt][ijet])
                jvec = jet.vector
                jdir = jvec / jvec.mag

                pPerp = []

                trks = LorentzVector()
                for tpt, teta, tphi in zip(output_trk_array['jet_trk_pt'][-1],
                                           output_trk_array['jet_trk_eta'][-1],
                                           output_trk_array['jet_trk_phi'][-1]):

                    trk = LorentzVector()
                    trk.setptetaphim(tpt, teta, tphi, mpi)
                    tvec = trk.vector
                    t_perp = (tvec.cross(jdir)).mag

                    trks = trks + trk

                output_trk_array['jet_trk_pPerp'].append(np.array(pPerp))

                # Add the derived jet variables
                if 'vecSumTrkPt' in derived_jet_vars:
                    jetList = jetList + [trks.pt]
                    
                
                    
                output_jet_array.append(np.array(jetList+evtList).reshape(1,-1))

                # Return if desired
                if len(output_jet_array) >= nJets:
                    return output_jet_array, output_trk_array, \
                           jet_vars+evt_vars, trk_vars+derived_trk_vars

                # Break ougt of the jet loop if you have the desired # jets / event.
                if jetCounter == jetsPerEvt:
                    break

    return output_jet_array, output_trk_array, jet_vars+bhadron_vars+derived_jet_vars+evt_vars, trk_vars+derived_trk_vars


def saveData(output_jet_array, output_trk_array, jetVars, trkVars, nTrks,
             subDir, sort_flag, mode, dataDir='data/', save=True, tag='',trkSelection=''):
    '''

    Take lists of the arrays for the jet and trk variables,
    truncates and pads the arrays to fixed length, creates a
    df for the jets and an xarray for the tracks, and saves
    them for future analysis.


    Inputs:
    - output_jet_array: A list of np arrays for the jet variables extracted
                        from the Ntuple
    - output_trk_array: A list of 2d np arrays for the track n_variables
    - jetVars: The names for the jet leafs we extracted from the Ntuple
    - trkVars: Names of the trk leafs we extracted from the Ntuple
    - nTrks: The number of tracks to truncate / pad the sequences to
    - subDir: Folder inside data/ to save the output to
    - sort_flag: The flag to append to the datafile to represent the sorting
                 scheme used
    - mode: Str to append to filename for whether you're processing train, test, or all ('') the data
    - save: (default True) whether to save the df + xr files or not
    - tag: Extra tag to append to the df and xr, if desired

    Outputs:
    - jet_df: A df for the jet variables where the rows represent different jets
              and the columns represent the jet variables.
    - trk_xr: An xarray for the track variables. Note: trk_xr.values is an
              np.array of shape (nJets, nTrks, nTrkVariables)
    '''

    # Concatenate lists to get numpy arrays
    jet_np = np.concatenate(output_jet_array,axis=0)
    print("jet_np",jet_np.shape)

    # Need to truncate / pad to consistent # of jets before concatenating
    trk_vars_flat = []
    for v in trkVars:
        padded = pad_sequences(output_trk_array[v], maxlen=nTrks,
                               padding='post', truncating='post',dtype=np.float32)
        trk_vars_flat.append(padded.reshape(-1,nTrks,1))

        print(v,trk_vars_flat[-1].shape)

    trk_np = np.concatenate(trk_vars_flat,axis=2)
    print("trk_np",trk_np.shape)

    # Put the jet np array in a df.
    # Since the "jet_" prefix is redundant, don't bother putting it in
    jet_cols = [c[4:] if 'jet_' in c else c for c in jetVars]
    jet_df = pd.DataFrame(jet_np, columns=jet_cols)

    # Put the trk np array in an xarray
    # Since the "jet_trk_" prefix is redundant, don't bother putting it in
    trk_cols = [c[8:] for c in trkVars]
    # Additionally: rename a couple of the variables
    # ip3d_d0sig -> sd0
    # ip3d_z0sig -> sz0
    trk_cols[0] = "sd0"
    trk_cols[1] = "sz0"

    trk_xr = xr.DataArray(trk_np,
                          coords=[('jet',np.arange(len(output_jet_array))),
                                  ('trk',np.arange(nTrks)),
                                  ('var',trk_cols)])

    jPtCols = [v for v in ['pt','pt_orig','m'] if v in jet_cols]
    jet_df[jPtCols] = jet_df[jPtCols].apply(MeVtoGeV)

    for v in ['pt','pPerp']:
        if v in trk_cols:
            trk_xr.loc[:,:,v] = MeVtoGeV(trk_xr.loc[:,:,v])

    #myTrkSelection = "_"+trkSelection if len(trkSelection) > 0 else trkSelection

    # Save the df + xarray
    if save:
        #data_tag = "{}{}_{}trks{}_{}{}".format(nJetsTag(len(output_jet_array)),mode,nTrks,myTrkSelection,sort_flag,tag)
        data_tag = "{}{}_{}trks_{}{}".format(nJetsTag(len(output_jet_array)),mode,nTrks,sort_flag,tag)
        jet_df.to_hdf('{}/{}/jet_{}.h5'.format(dataDir,subDir,data_tag), key='jet_df',mode='w')
        trk_xr.to_netcdf('{}/{}/trk_{}.nc'.format(dataDir,subDir,data_tag))

    return jet_df, trk_xr


def loadData(filename, nJets, subDir="", myTag="",nTrks=15,outputDir=None):
    '''
    Load in the df and xarray object saved from getData
    Inputs:
    - filename: The name of the .h5 or .nc files that we're trying to process
    - nJets: The number of jets to put inside the new file
    - subDir: If not an empty string, specifies where in data/ to save the output to.
              If an empty string is passed, the output dir will be inferred from the input file.
    - myTag: If not an empty string, the data tag to append to the jet and trk xrs
    '''

    fileList = glob.glob(filename)
    print("{} files passed as input".format(len(fileList)))

    for i, fname in enumerate(fileList):

        print("{}. {}".format(i+1,fname))

        # Step 1: Extract the name + path to the file from the full file name + path
        name = fname.split('/')[-1]
        dataDir = '/'.join(fname.split('/')[:2]) if len(subDir) == 0 else "data/"+subDir
        path = '/'.join(fname.split('/')[:-1])

        # Step 2: Open the files
        if 'jet' in fname: #) or ('trk' in fname):
            data_tag = name[4:-3]
            jet_df_tmp = pd.read_hdf('{}/jet_{}.h5'.format(path,data_tag), key='jet_df')
            trk_xr_tmp = xr.open_dataarray('{}/trk_{}.nc'.format(path,data_tag))
        else:
            f = h5py.File(fname,'r')
            jet_np = f['jets'][:]
            trk_np = f['tracks'][:]

            # Convert to dfs and xarrays that the rest of the script works with
            jet_df_tmp = pd.DataFrame(jet_np)
            col_map = {k : k.replace('uncalib','orig') for k in jet_np.dtype.names}
            col_map["HadronConeExclTruthLabelID"] = "LabDr_HadF"
            jet_df_tmp.rename(index=int,columns=col_map,inplace=True)

            arrs = [trk_np[x] for x in trk_np.dtype.names]
            vals = np.stack(arrs,axis=-1)
            vals = np.nan_to_num(vals)

            if nTrks < trk_np.shape[1]:
                # Truncate the array
                vals = vals[:,:nTrks,:]

            N,timesteps,nFeatures = vals.shape
            trk_cols = [var_map[x] if x in var_map.keys() else x for x in list(trk_np.dtype.names)]

            removeEls = ["chiSquared","numberDoF","IP2D_grade","IP3D_grade","deta","dphi","ip2d_d0","ip3d_d0","ip3d_z0"]
            idx = np.array([i for i,x in enumerate(trk_cols) if not (x in removeEls)])
            trk_cols = [x for x in trk_cols if x not in removeEls]

            for c in trk_cols:
                print(c)
            trk_xr_tmp = xr.DataArray(vals[:,:,idx],coords=[('jet',np.arange(N)),
     											   ('trk',np.arange(timesteps)),
                                                   ('var',trk_cols)])


            # When starting from Manuel's setup, the data have not yet have the pts scaled from MeV -> GeV
            jPtCols = [v for v in ['pt','pt_orig','m'] if v in jet_df_tmp.columns]
            jet_df_tmp[jPtCols] = jet_df_tmp[jPtCols].apply(MeVtoGeV)
            trk_xr_tmp.loc[:,:,'pt'] = MeVtoGeV(trk_xr_tmp.loc[:,:,'pt'])

            # The dl1 hdf5 files have the default IP3D_grade IP2D_grade variables set to -1
            # So by adding 1 to these columns, we fix the mask and have the categories at
            # the correct values for masking
            if ("IP2D_grade" in trk_cols) and ("IP3D_grade" in trk_cols):
                trk_xr_tmp.loc[:,:,'IP2D_grade'] += 1
                trk_xr_tmp.loc[:,:,'IP3D_grade'] += 1

        if i == 0:
            jet_df = jet_df_tmp
            trk_xr = trk_xr_tmp
        else:
            jet_df = pd.concat([jet_df,jet_df_tmp],ignore_index=True)
            trk_xr = xr.concat([trk_xr,trk_xr_tmp],dim='jet')

        if jet_df.values.shape[0] >= nJets:
            break

    # Select the requested number of jets
    jet_df = jet_df[:nJets]
    trk_xr = trk_xr[:nJets]

    # If this dataframe was created by concatenating others, save it for future ref
    if (dataDir != path) or (outputDir is not None):

        print("myTag",myTag)

        if len(myTag) == 0:
            tags = name.split('_')
            jTag = nJetsTag(jet_df.values.shape[0])
            if 'test' in tags[1]:
                tags[1] = "{}test".format(jTag)
            elif 'train' in tags[1]:
                tags[1] = "{}train".format(jTag)
            else:
                tags[1] = "{}".format(jTag)
            data_tag = "_".join(tags[1:-1])
        else:
            data_tag = myTag

        print("Saving the output data files:")
        print('  {}/jet_{}.h5'.format(dataDir,data_tag))
        print('  {}/trk_{}.nc'.format(dataDir,data_tag))

        jet_df.to_hdf('{}/jet_{}.h5'.format(dataDir,data_tag), key='jet_df',mode='w')
        trk_xr.to_netcdf('{}/trk_{}.nc'.format(dataDir,data_tag))

    return jet_df, trk_xr

def trkSelection(jet_df, trk_df, nFixedTrks=-1, trkIPSgn="all"):
    '''

    Can either select jets with a fixed number of tracks, or do
    the IP sign selection.

    Jet Cuts:
        - If nFixedTrks != -1, select trks w/ this many tracks

    Trk Cuts:
        - If trkIPSgn is not all, selects the sign for the trk IP
          based on the flag passed by trkIPSgn

    Inputs:
        jet_df: df with the jet variables
        trk_df: df with the trk variables
        nFixedTrks: If we're only selecting jets with a fixed # of trks
        trkIPSgn: If not all, the sign of the IP to select for the tracks.
                  Useful for the flipped tagger studies

    Outputs:
    	jet_df
        trk_df

    '''


    if nFixedTrks != -1:

        print('Jets before requiring {} tracks: {}'.format(nFixedTrks, len(jet_df.index)))

        trkMult = pd.Series([trks.size for trks in trk_df.sd0_arr])
        nTrksMask = (trkMult == nFixedTrks)

        jet_df = jet_df[nTrksMask]
        jet_df.reset_index(inplace=True,drop=True)

        trk_df = trk_df[nTrksMask]
        trk_df.reset_index(inplace=True,drop=True)

        print('Jets after requiring {} tracks: {}'.format(nFixedTrks, len(jet_df.index)))

    assert trkIPSgn == "all" # Need to refactor this code still

    # I think the df inputs are NOT passed by reference
    return jet_df, trk_df


def pTReweight(jet_df, rwtDist='l'):
    '''
    Do the pT reweighting for the sample, and add a new col "sample_weight" to the jet_df

    Inputs:
    - jet_df: pandas dataframe w/ the jet level variables
    - rwtDist: The flavor spectrum to reweight to: l (default), c, or b

    '''

    assert (rwtDist == 'l') or (rwtDist == 'c') or (rwtDist == 'b')

    # For consistency, use the same values that Zihao used
    # start, stop, step = 0, 3000, 10 # GeV

    # For the extended hybrid training, I thought I needed more bins
    start, step = 0, 10 # GeV
    stop = np.max(jet_df.pt_orig)

    pT_edges = np.arange(start, stop+step, step)

    # Get the bin that each entry corresponds to
    x_ind = np.digitize(jet_df.pt_orig, pT_edges) - 1

    # Make the histograms
    l_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==0].pt_orig, bins=pT_edges)
    c_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==4].pt_orig, bins=pT_edges)
    b_hist, _ = np.histogram(jet_df[jet_df.LabDr_HadF==5].pt_orig, bins=pT_edges)

    # Normalize and add epsilon so that you never get a divide by 0 error
    epsilon = 1e-8
    l_hist = l_hist / np.sum(l_hist) + epsilon
    c_hist = c_hist / np.sum(c_hist) + epsilon
    b_hist = b_hist / np.sum(b_hist) + epsilon

    # Reweight the b-jets to have the pT and eta dist as bkg
    if rwtDist == 'l':
        w = [l_hist[ix] / b_hist[ix] if pdg == 5 else l_hist[ix] / c_hist[ix] if pdg == 4 else 1  \
             for pdg, ix in zip(jet_df.LabDr_HadF, x_ind)]
    elif rwtDist == 'c':
        w = [c_hist[ix] / b_hist[ix] if pdg == 5 else c_hist[ix] / l_hist[ix] if pdg == 0 else 1  \
             for pdg, ix in zip(jet_df.LabDr_HadF, x_ind)]
    elif rwtDist == 'b':
        w = [b_hist[ix] / l_hist[ix] if pdg == 0 else b_hist[ix] / c_hist[ix] if pdg == 4 else 1  \
             for pdg, ix in zip(jet_df.LabDr_HadF, x_ind)]
    else:
        print('Error: {} not recognized for the reweighting spectrum.')

    jet_df['sample_weight'] = pd.Series(w)


def scale(data, var_names, savevars, filename, mask_value=0,
          jetData=None,jet_names=[]):
    '''
    Args:
    -----
        data: a numpy array of shape (nb_events, nb_particles, n_variables)
        var_names: list of keys to be used for the model
        savevars: bool -- True for training, False for testinh
                  it decides whether we want to fit on data to find mean and std
                  or if we want to use those stored in the json file
        VAR_FILE: string: Where to save the output
        mask_value: the value to mask when taking the avg and stdev

    Returns:
    --------
        modifies data in place, writes out scaling dictionary

    Reference: Taken from Micky's dataprocessing.py file in
    https://github.com/mickypaganini/RNNIP
    '''
    import json

    scale = {}

    # Track variables
    # data has shape nJets,nTrks,nFeatures,so to sort out the mask,
    # we need to find where the value is masked for a track over
    # all it's features
    # mask has shape nJets,nTrks
    mask = ~ np.all(data == mask_value, axis=-1)

    if savevars:

        for v, name in enumerate(var_names):
            print('Scaling feature {} of {} ({}).'.format(v + 1, len(var_names), name))
            f = data[:, :, v]
            # slc = f[f != mask_value]
            slc = f[mask]
            m, s = slc.mean(), slc.std()
            slc -= m
            slc /= s
            # data[:, :, v][f != mask_value] = slc.astype('float32')
            data[:, :, v][mask] = slc.astype('float32')
            scale[name] = {'mean' : float(m), 'sd' : float(s)}

        # Jet variables
        if jetData is not None:
            scaler = StandardScaler(copy=False) # scale the data in place
            scaler.fit_transform(jetData)
            scale['jet_mean'] = scaler.mean_
            scale['jet_scale'] = scaler.scale_
            scale['jet_var'] = scaler.var_
            scale['n_samples_seen'] = scaler.n_samples_seen_

        with open(filename, 'w') as varfile:
            json.dump(scale, varfile)

    else:
        with open(filename, 'r') as varfile:
            varinfo = json.load(varfile)

        for v, name in enumerate(var_names):
            print('Scaling feature {} of {} ({}).'.format(v + 1, len(var_names), name))
            f = data[:, :, v]
            #slc = f[f != mask_value]
            slc = f[mask]
            m = varinfo[name]['mean']
            s = varinfo[name]['sd']
            slc -= m
            slc /= s
            #data[:, :, v][f != mask_value] = slc.astype('float32')
            data[:, :, v][mask] = slc.astype('float32')

        if jetData is not None:
            scaler = StandardScaler(copy=False) # scale the data in place
            varinfo['jet_mean'] = scaler.mean_
            varinfo['jet_scale'] = scaler.scale_
            varinfo['jet_var'] = scaler.var_
            varinfo['n_samples_seen'] = scaler.n_samples_seen_
            scaler.transform(jetData)

def prepareForKeras(jet_df, trk_xr, jetTag,
                    noNormStr, logNormStr, jointNormStr,jetVars='',
                    nTrkClasses=0, mode='', scalingfile='', scale_tag='',
                    subDir='mc16d_Topo_ttbar', sortFlag="sd0_rev", ipTag="",
                    rwtTag="",trkMask=None):
    '''
    Prepare the train and test inputs / labels for Keras, and save them as h5py files
    in the "data/" directory.

    NOTE: I also want to put the selection of the variables that I'm using for the network here!

    Inputs:
    - jet_df: The dataframe with the jet level vars
    - trk_xr: The xarray with the trk level vars
    - jetTag: A tag representing how many jets and trks are in the dataset

    - noNormStr: A string of a '_' separated list of the variable names that we
                 are not going to normalize.
    - logNormVars: Variables that we take the log of before normalizing.
    - jointNormVars: Variables that we want to normalize
    - jetVars: A list for the jet variables to include in the network

    - nTrkClasses: The number of track classes to use for the trk truth labels.
                   If 0, do not save y_trk or the ntrks_ip3d to the hdf5 file
    - mode: Whether we're running in train or test files. If an empty string
            is passed (default), the train / test split will be performed here.
    - scalingfile: If not empty str, can open a previously saved different scaling file

    - scale_tag: A extra tag to append to the hdf5 file (if desired).
    - subDir: Folder inside data/ to save the output to
    - sortFlag: The applied track sort (default sd0_rev)
    - ipTag: A tag to append to the datafile for what signage selection was used
             for the files. An empty string uses all of the tracks, while "pos"
             (neg) selects only those with postive (negative) IPs.
    - rwtTag: The tag specifying which dist was used to reweight the others to

    '''

    assert (nTrkClasses==0) or (nTrkClasses == 4) or (nTrkClasses == 5) # only functionality for these trk labels atm

    # Step 0: Process the string inputs for the vars in each norm sheme -> list
    noNormVars = strToList(noNormStr)
    logNormVars = strToList(logNormStr)
    jointNormVars = strToList(jointNormStr)

    # Step 1: Select the relevant variables
    inpts = noNormVars + logNormVars + jointNormVars

    # Check that all of the requested inputs are actually vars in trk_xr
    trkInputs = list(trk_xr.coords['var'].values)
    for inpt in inpts:
        if inpt not in trkInputs:
            raise ValueError('In prepareForKeras(): requested var {} not in trk_xr'.format(inpt))

    X = trk_xr.loc[:,:,inpts].values
    X_jet = jet_df[jetVars]
    ix = trk_xr.indexes['jet']
    print("X.shape = ", X.shape)

    # Sometimes I might want to apply an additional selection that can mask
    # some of the tracks
    if trkMask is not None:
        X[~trkMask] = 0
        Xnew = X.copy()
        del X
        X = Xnew

    # Keep track of which tracks in the jet are masked
    mask = ~ np.all(X == 0, axis=-1)
    #if trkMask is not None:
    #    mask = mask & trkMask
    print("mask",mask.shape)

    # trk_grade hack
    if 'ip3d_grade' in inpts:
        # Since the valid categories are from 0 - 13, need to shift the values
        # so that the embedding layer can mask with 0s
        #trk_xr.loc[:,:,'ip3d_grade'] = xr.where(mask, trk_xr.loc[:,:,'ip3d_grade']+1, 0)
        iloc = inpts.index['ip3d_grade']
        X[:,:,iloc] = xr.where(mask, X[:,:,iloc]+1, 0)


    if nTrkClasses != 0:
        # If you're only training with 4 track classes, need to combine the b+c HF
        # tracks from the HF variable
        y_trk = trk_xr.loc[:, :, 'orig'].values
        if nTrkClasses == 4:
            for ti_new, ti_old,  in enumerate([1,2,3,-1]):
                y_trk = np.where(y_trk==ti_old, ti_new, y_trk)
        trkTag = "_{}trkClasses".format(nTrkClasses)
    else:
        trkTag = ""

    # Take the log of the desired variables
    for i, v in enumerate(logNormVars):
        j = i + len(noNormVars)
        X[:,:,j][mask] = np.log(np.where(X[:,:,j][mask]==0,1e-8,X[:,:,j][mask]))

    # Step 2: Train / test split

    pdg_to_class = {0:0, 4:1, 5:2, 15:3}
    y = jet_df.LabDr_HadF.replace(pdg_to_class).values

    if len(mode) == 0:
        random_seed = 25
        X_train, X_test, y_train, y_test, ix_train, ix_test, w_train, w_test, = \
            train_test_split(X, y, ix, jet_df.sample_weight, test_size=0.333,
                             random_state=random_seed)

    # Step 3: Normalize the requested inputs

    # Get a string representing the variables getting scaled
    varTag = "_".join(noNormVars) if len(noNormVars) != 0 else ''
    varTag += '_logNorm_' + "_".join(logNormVars) if len(logNormVars) != 0 else ''
    varTag += '_norm_' + "_".join(jointNormVars) if len(jointNormVars) != 0 else ''

    # Scale the vars and save the files
    if len(scalingfile) == 0:
        scalingfile = "data/{}/scale_{}_{}{}{}.json".format(subDir,jetTag,varTag,ipTag,rwtTag)
        scalingfile = scalingfile.replace('test','train')
    print("scalingfile",scalingfile)

    if len(mode) == 0:

        if len(logNormVars)+len(jointNormVars) > 0:

            scale(X_train[:,:,len(noNormVars):], logNormVars+jointNormVars, savevars=True,  filename=scalingfile)
            scale(X_test[:,:, len(noNormVars):], logNormVars+jointNormVars, savevars=False, filename=scalingfile)

        myDict = {
            "X_train" : X_train,
            "y_train" : y_train,
            "ix_train" : ix_train,
            "weights_train" : w_train,

            "X_test" : X_test,
            "y_test" : y_test,
            "ix_test" : ix_test,
            "weights_test" : w_test,
        }


    elif mode == 'train':
        if len(logNormVars)+len(jointNormVars) > 0:
            scale(X[:,:,len(noNormVars):], logNormVars+jointNormVars, savevars=True,  filename=scalingfile)

        myDict = {
            "X_train" : X,
            "y_train" : y,
            "ix_train" : ix,
            "weights_train" : jet_df.sample_weight
        }

    elif mode == 'test':
        if len(logNormVars)+len(jointNormVars) > 0:
            scale(X[:,:, len(noNormVars):], logNormVars+jointNormVars, savevars=False, filename=scalingfile)

        myDict = {
            "X_test" : X,
            "y_test" : y,
            "ix_test" : ix,
        }

    if nTrkClasses != 0:
        if mode == 'train' or len(mode) == 0:
            myDict['y_trk_train'] = y_trk if mode == 'train' else y_trk[ix_train]
            myDict['nTrks_train'] = nTrks if mode == 'train' else nTrks[ix_train]
        if mode == 'test' or len(mode) == 0:
            myDict['y_trk_test'] = y_trk if mode == 'test' else y_trk[ix_test]
            myDict['nTrks_test'] = nTrks if mode == 'test' else nTrks[ix_test]

    if len(jetVars) > 0:
        if mode == 'train' or len(mode) == 0:
            myDict['X_jet_train'] = X_jet if mode == 'train' else X_jet[ix_train]
        if mode == 'test' or len(mode) == 0:
            myDict['X_jet_test']  = X_jet if mode == 'test'  else X_jet[ix_test]


    # Step 4: Save as h5py files
    outputFile = 'data_{}_{}_{}{}{}{}{}.hdf5'.format(jetTag,varTag,sortFlag,ipTag,rwtTag,trkTag,scale_tag)
    print("Saving datasets in {}".format(outputFile))
    f = h5py.File('data/{}/{}'.format(subDir,outputFile), 'w')

    for key, val in myDict.items():
        f.create_dataset(key, data=val)

    f.close()

if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--nMaxTrack','--nTrks', type=int, default=15, dest="nMaxTrack",
                   help="Maximum number of tracks (default 15)")
    p.add_argument('--nFixedTrks', type=int, default=-1, dest="nFixedTrks", help="Only select jets with this many tracks (default -1 => use the nMaxTracks to pad the sequence instead)")
    p.add_argument('--trkIPSign','--trkIPSgn', type=str, default='all', dest="trkIPSgn",
                   help="The sign of the IP to select for the tracks: all (default), pos / positive, or neg / negative")
    p.add_argument('--nJets', type=int, default=int(3e6), dest="nJets",
                   help="Number of jets to process (default 3m)")
    p.add_argument('--rwtDist',type=str,default='l',dest="rwtDist",
                   help="The flavor spectrum to reweight the pT to: l (default), c, or b")

    p.add_argument('--filename', type=str,
                   default="/gpfs/slac/atlas/fs1/d/rafaeltl/public/RNNIP/FTAG_ntups/user.rateixei.mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.AOD..dijetSamplesNominal20180629_Akt4EMTo/user.rateixei.14594351.Akt4EMTo._000*.root",
                   dest="filename",
                   help="Path to and name of the root (or .h5 / .nc) files")

    # The info for which subdir to save the output file to
    p.add_argument("--mc", type=str, default="mc16d", dest="mc",
                   help="mc version: mc16a, mc16d (default), mc16e")
    p.add_argument('--jetCollection', type=str, default='Topo', dest="jetCollection",
                   help="Jet collection to train over: Topo (default), PFlow, VR")
    p.add_argument('--physicsSample', type=str, default='ttbar', dest="physicsSample",
                   help="the physics sample of interest: can be one of ttbar (default),"
                   +"Zprime_1.5TeV, Zprime_5TeV, hybrid_1.5TeV, hybrid_5TeV")

    # Additionally, for testing new configurations for the input variables, I also want to include the
    # functionality to save the output to a non-standard location, and append my own data_tag
    p.add_argument('--subDir', type=str, default="",
                   help="directory inside data/ to save the data to. "\
                   +"If this variables is empty string (default), derive subDir using the mc, jetCollection, and physicsSample args.")
    p.add_argument('--data_tag', type=str, default="",
                   help="Tag to append to the jet and trk df and xarrays in loadData."\
                   +" If an empty string is passed, save the output using the input filename structure.")


    p.add_argument('--sortFlag', type=str, dest="sortFlag", default="sd0_rev",
                   help="Sorting configuration for the tracks, default sd0_rev.")
    p.add_argument('--nTrkClasses', type=int, default=0, dest='nTrkClasses', help="# of trk classes")

    p.add_argument('--trkSelection',type=str,dest="trkSelection",default='',
                   help="The track selection to use: the default is the same "\
                   +"as ip3d, but a string like pt_700_d0_2_z0sinTheta_6 "\
                   +"will be interpretted as pT > 700 MeV, |d0| < 2 mm, "\
                   +"|z0*sin(theta)| < 6 mm")

    p.add_argument('--noNormVars', type=str, default='sd0,sz0,nNextToInnHits,nInnHits,'\
                   +'nsharedBLHits,nsplitBLHits,nsharedPixHits,nsplitPixHits,nsharedSCTHits',
                   help='Variables not to normalize: Pass -1 for an empty list')
    p.add_argument('--logNormVars', type=str, default='ptfrac,dr',
                   help='Variables to take the log of before normalizing, '\
                   +'default ptfrac, dr: Pass -1 for an empty list')
    p.add_argument('--jointNormVars', type=str, default='nPixHits,nSCTHits',
                   help='Variables to whiten: Pass -1 for an empty list')
    p.add_argument('--jetVars',type=str, default="-1", help="Jet variables to pass to the network")

    p.add_argument('--mode', type=str, default='',help='Mode for processing the data: \n'\
                   +'  default (''): process this many jets, and perform the train/test split\n'\
                   +'  train: process only the training jets, do the pT reweighting and scaling on all jets\n'\
                   +'  test: process only the test jets, loading in the scalingfile from the scalingfile arg\n')

    p.add_argument('--scalingfile', type=str, default='',help='String for loading in the scaling tag'
                   +' for running in test mode. If this is an empty string and you\'re running in'
                   +' the testing mode, it assumes that the same mc, jetCollection, and physicsSample'
                   +' should be used for the scaling and will recreate the appropriate scaling name to'
                   +' open the relevant file')

    p.add_argument('--scale_tag', type=str,default='',help='tag to append to the test file when a different'
                  +'scaling configuration was used.')

    p.add_argument('--timestamp',type=str,default='',help='timestamp for reading the TTree')

    args = p.parse_args()


    # Unpack the arguments
    mode = args.mode
    nJets = args.nJets
    nTrks = args.nMaxTrack
    nFixedTrks = args.nFixedTrks

    filename = args.filename
    sortFlag = args.sortFlag
    jetCollection = args.jetCollection
    timestamp = args.timestamp
    physicsSample = args.physicsSample
    trkSelection = args.trkSelection

    subDir = "{}_{}_{}".format(args.mc,jetCollection,physicsSample) if len(args.subDir) == 0 else args.subDir
    if len(trkSelection) > 0:
        subDir += "_{}".format(trkSelection)

    print(subDir)

    # If this subDir does not yet exist, create it
    dataDir = "data/"+subDir
    if not os.path.exists(dataDir):
        print(dataDir+" does not exits: creating new directory, and the corresponding ones in models and figures as well.")
        os.mkdir("data/"+subDir)
        os.mkdir("models/"+subDir)
        os.mkdir("figures/"+subDir)

    # Check some of the arguments validity
    print(mode)
    assert (mode == 'train') or (mode == 'test') or (len(mode) == 0)

    # Sanity check that you're running w/ a sensible combination of parameters
    if args.trkIPSgn != 'all' and sortFlag == 'sd0_absrev':
        print('WARNING: If trkIPSgn == {}, are you sure you want to run w/ |sd0| sort?')

    # Load in the np arrays from Keras
    if ".root" in filename:
        print("Processing data from root files: calling getData() and saveData()")


        # Below are the jet and trk variables for accessing the events out of the root file
        jet_vars = ["jet_LabDr_HadF","jet_pt_orig","jet_eta_orig","jet_phi_orig","jet_m",
                    "jet_ip3d_llr","jet_ip3d_pb","jet_ip3d_pc","jet_ip3d_pu",
                    "jet_aliveAfterOR","jet_aliveAfterORmu",
                    "jet_JVT","jet_pt","jet_eta","jet_phi",
                    "jet_nBHadr","jet_nCHadr"] #,
                    #"jet_iprnn_pu","jet_iprnn_pc","jet_iprnn_pb","jet_iprnn_ptau"]

        bhadron_vars = ["jet_bH_pt","jet_bH_eta","jet_bH_Lxy","jet_bH_dRjet",
                        "jet_bH_PtTrk","jet_bH_MTrk","jet_bH_nBtracks",
                        "jet_bH_nCtracks","jet_bH_nBtracks_400","jet_bH_nCtracks_400"]

        derived_jet_vars = ["vecSumTrkPt","nTrk","nTrk_PU","nTrk_HF","nTrk_frag","nTrk_GEANT"]

        trk_vars  = ["jet_trk_ip3d_d0sig", "jet_trk_ip3d_z0sig",
                    "jet_trk_ip3d_d0", "jet_trk_ip3d_z0","jet_trk_ip2d_d0",
                    "jet_trk_ip3d_grade", #"jet_trk_ip3d_llr",
                    "jet_trk_pt", "jet_trk_eta", "jet_trk_theta", "jet_trk_phi",
                    "jet_trk_orig",
                    "jet_trk_nInnHits","jet_trk_nsharedBLHits","jet_trk_nsplitBLHits",
                    "jet_trk_nNextToInnHits",
                    "jet_trk_nPixHits","jet_trk_nPixHoles",
                    "jet_trk_nsharedPixHits","jet_trk_nsplitPixHits",
                    "jet_trk_nSCTHits","jet_trk_nSCTHoles",
                    "jet_trk_nsharedSCTHits",
                    'jet_trk_ndeadPixSensors','jet_trk_ndeadSCTSensors'
                    ]
        derived_trk_vars = ['jet_trk_ptfrac','jet_trk_dr',
                           'jet_trk_prbP','jet_trk_pPerp']

        if jetCollection == "VR": jet_vars += ["jet_nConst"]

        if mode == 'train' and len(args.trkSelection) == 0:
            # Some extra variables that aren't in Francesco's hybrid Ntuple
            evt_vars = []
        else:
            evt_vars = ['avgmu']
            jet_vars = jet_vars + ["jet_ip3d_ntrk","jet_rnnip_pu","jet_rnnip_pc","jet_rnnip_pb","jet_rnnip_ptau"]
            #trk_vars = trk_vars + ["jet_trk_ip3d_d0", "jet_trk_ip3d_z0","jet_trk_ip2d_d0"] #,"jet_trk_algo"]

        if not ('pt' in trkSelection and 'd0' in trkSelection and 'z0' in trkSelection):
            print("Using ip3d selection",args.trkSelection)
        else:
            print("Trk selection",args.trkSelection)

        jet_list, trk_list, jetVars, trkVars  = getData(filename, nJets, jetCollection, timestamp, sortFlag,
                                                        jet_vars, bhadron_vars, derived_jet_vars, evt_vars, trk_vars, derived_trk_vars)#,\
                                                        trkSelection=args.trkSelection)
        jet_df, trk_xr  = saveData(jet_list, trk_list, jetVars, trkVars, nTrks, subDir, sortFlag, mode)
                                   trkSelection=args.trkSelection)

    elif ".h5" in filename or ".nc" in filename:
        jet_df, trk_xr  = loadData(filename=filename, nJets=nJets, subDir=subDir, myTag=args.data_tag, nTrks=nTrks)
        print(jet_df.columns)
    else:
        print('Error: Unrecognizable format for filename',filename)

    # Apply any additional trk cuts
    assert nFixedTrks == -1 and args.trkIPSgn == 'all'
    # jet_df, trk_xr = trkSelection(jet_df, trk_xr, args.nFixedTrks, args.trkIPSgn)

    # Get the flattened pT dist
    if mode != 'test':
        pTReweight(jet_df, args.rwtDist)

    # Do the train / test split and save the files
    jetTag = nJetsTag(len(jet_df.index))
    jetTag += mode

    jetVars = strToList(args.jetVars)
    nJetVars = len(jetVars)
    if len(jetVars) > 0:
        jetTag += "_{}".format("_".join(jetVars))

    if nFixedTrks == -1:
        jetTag += "_{}trks".format(nTrks)
    else:
        jetTag += "_ONLY_{}trks".format(nFixedTrks)
    ipTag = getIPTag(args.trkIPSgn)

    rwtTag = '' if args.rwtDist == 'l' else '_rwt-{}'.format(args.rwtDist)
    print("rwtTag = {}".format(rwtTag))

    #myTrkSelection = "_"+args.trkSelection if len(args.trkSelection) > 0 else args.trkSelection
    #jetTag += myTrkSelection

    prepareForKeras(jet_df, trk_xr, jetTag,
                    args.noNormVars, args.logNormVars, args.jointNormVars, jetVars,
                    args.nTrkClasses, mode, args.scalingfile, args.scale_tag,
                    subDir, args.sortFlag, ipTag, rwtTag)
