'''

Goal: I have been drawing roc curves using a script which loops through the
training and test samples sequentially to draw a sequence of roc curves, but
this was a limiting factor in my turn around time, so I'm going to write a
script so that I can submit these jobs in parallel.

'''

def run_one():
    '''

    '''

if __name__ == '__main__':

    from argparse import ArgumentParser
    p.add_argument('--jetCollection', type=str, default='VR',
                   help="Jet collection to use for the training and evaluation.")
    p.add_argument('--batch', action="store_true",
                   help="Whether to run these commands interactivately or submit them to the LSF batch system.")
    p.add_argument('--verbose', action="store_true", help="Print the cmd before running it")
    p.add_argument('--trainPhysicsSamples', type=str,
                   default="ttbar,hybrid_1.5TeV_cut_125GeV,hybrid_5TeV_cut_125GeV,Zprime_1.5TeV,Zprime_5TeV",
                   help="Comma separated list of the physics samples to use for "\
                   +"the training models.")
    p.add_argument("--miniJetTags", type=str, default="3m",
                   help="Comma separated list of the jet tags for each of the "\
                   +"training physics samples. Must be the same length as "\
                   +"trainPhysicsSamples. Default 3e6.")
    p.add_argument('--testPhysicsSamples', type=str, default="ttbar,Zprime_1.5TeV_2lead,Zprime_5TeV_2lead",
                   help="Comma separated list of the physics samples to use for "\
                   +"each test file.")

    p.add_argument('--trainNTrks',type=str,default='15,15,15,25,25',help="The number of tracks used for "\
                  +"training. Can either be a single value (defualt 15) or a comma separated "\
                  +"list to use a different # of tracks for each training physics sample.")
    p.add_argument('--testNTrks',type=str,default='15,25,25',help="The number of tracks used for "\
                   +"testing. Also can either be a single value or a comma separated "\
                   +"list to use a different # of tracks for each training physics sample.")

    p.add_argument('--modelName',type=str,default='',help='If passed, will make the roc curves for '\
                   +'a single file')

    args = p.parse_args()
