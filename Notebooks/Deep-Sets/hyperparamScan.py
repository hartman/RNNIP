'''
Script to submit some initial trainings for a first coarse hyperparameter scan
for the deep sets models.

Nicole Hartman
Dec 2018

'''
import os

inputArgs = {
	'mc':'mc16d',
    'jetCollection':'PFlow',
	'physicsSample':'ttbar',
	'model':'PFN',
    'nJets':int(2e6),
    'mode':'train',

}

fout = open("../../deepSet_tmp.sh","w")
fout.write("#!/bin/bash -u\n\n")

os.chdir("/u/ki/nhartman/gpfs/RNNIP")

i = 1
for dense_layers in [1,2,3]:
	for dense_size in [50, 100]:

		inputArgs['dense_sizes'] = ",".join([str(dense_size)]*dense_layers)

		for latent_dim in [64,128]:                                                      					
	
			for ppm_layers in [0,1,2]:                                                               						
				for ppm_size in [50, 100]:

					# It doesn't make sense to train multiple duplicate models 
					if ppm_layers == 0 and ppm_size > 50:
						continue

					inputArgs['ppm_sizes'] = ",".join([str(ppm_size)]*ppm_layers + [str(latent_dim)])							

					job_name = 'phi_{}_F_{}'.format(inputArgs['ppm_sizes'],inputArgs['dense_sizes'])
					cmd = "python trainNet.py"
					for key, val in inputArgs.items():
						cmd += " --{} {}".format(key,val)

					print("{}. {}".format(i, cmd))
					#fout.write('bsub -W 80:00 -q atlas-t3 -oo "output/' + job_name + '.txt" -J "' + job_name + '" "' + cmd + '"\n')
					fout.write(cmd + '\n')
					#os.system(cmd)
					i += 1

fout.close()
os.system("chmod a+x deepSet_tmp.sh")


