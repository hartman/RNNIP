'''
Draw ROC curves... fast
'''
import numpy as np
import h5py
import sys
from trainNet import *

baseSubDir = 'mc16d_PFlow_ttbar'
dataDir = "data/"
modelDir = 'models/'
figDir = 'figures/'

nTrks = 40
nFeatures = 13

dense_sizes = [100] * 3
ppm_sizes = [100,100,128]

vStr = "sd0_sz0_nNextToInnHits_nInnHits_nsharedBLHits_nsplitBLHits_nsharedPixHits_nsplitPixHits_nsharedSCTHits_logNorm_ptfrac_dr_norm_nPixHits_nSCTHits"

dataTagTrain = "3mtrain_{}trks_{}".format(nTrks,vStr)
dataTagTest  = "2mtest_{}trks_{}_abs_sd0_rev".format(nTrks,vStr)

for pt_cut in [1.0]:
	for d0_cut in [1.5]:
		for z0_cut in [1.5,3.0,5.0]:

			print("pt: {}, d0: {}, z0: {}".format(pt_cut, d0_cut, z0_cut))

			trkTag = "_pt_{}_d0_{}_z0_{}".format(pt_cut,d0_cut,z0_cut)
			subDir = baseSubDir+trkTag

			f_name = "{}/{}/data_{}.hdf5".format(dataDir,subDir,dataTagTest)
			f = h5py.File(f_name,"r")

			X = f['X_test'][:]
			y = f['y_test'][:]
			ix = f['ix_test'][:]

			f.close()

		    # Load in the model
			dips = DIPS(ppm_sizes, dense_sizes, timeSteps=nTrks, nFeatures=nFeatures,
			            modelDir=modelDir+subDir, dataTag=dataTagTrain, nClasses=3,
			            loadModel=True, dropout=0,batch_norm=True)

			if not os.path.exists(figDir+subDir):
			    os.mkdir(figDir+subDir)

			# Evaluate
			# trainingMetrics(dips.modelName, modelDir+subDir)
			# leff, ceff, beff = getEffs(dips, Xs[-1], ys[-1], ixs[-1], figDir=figDir,
			#                            subDir=subDir, modelDir=modelDir+subDir)
