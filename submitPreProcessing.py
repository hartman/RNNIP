'''

submitPreProcessing.py

This script looks for where I downloaded Binbin's Ntuples at SLAC,
and runs my root_to_np.py script on them.

'''
import os


NtupleDir = '/u/ki/nhartman/gpfs/public/'

Ntuples = {

    'mc16a_PFlow_ttbar' : 'user.bdong.410470.ttbar_hdamp258p75_nonallhad.mc16a_newDerivation_noRetag_Akt4EMPf/user.bdong.16267610.Akt4EMPf._00*[{}].root',
    'mc16a_PFlow_Zprime_1.5TeV' : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16a_newDerivation_noRetag_Akt4EMPf/user.bdong.16228899.Akt4EMPf._000*[{}].root',

    'mc16d_PFlow_ttbar'         : 'user.bdong.410470.ttbar_hdamp258p75_nonallhad.mc16d_newDerivation_noRetag_Akt4EMPf/user.bdong.16276417.Akt4EMPf._00*[{}].root',
    'mc16d_PFlow_Zprime_1.5TeV' : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16d_newDerivation_noRetag_rerun_Akt4EMPf/user.bdong.16288899.Akt4EMPf._000*[{}].root',
    'mc16d_PFlow_Zprime_5TeV'   : 'user.bdong.mc16_13TeV.427080.flatpT_Zprime.mc16d_newDerivation_noRetag_rerun_Akt4EMPf/user.bdong.16288899.Akt4EMPf._000*[{}].root',
    # 'mc16d_PFlow_hybrid_1.5TeV' : 'FrancescoNtuples/mc16d_PFlow_hybrid_1.5TeV/Rnn_Pflow51208.root',
    # 'mc16d_PFlow_hybrid_5TeV'   : 'FrancescoNtuples/mc16d_PFlow_hybrid_5TeV/Rnn_Pflow51208_extended_pT.root'
}

# Maybe later I can put in some fuctionality to get the filenames for the correct jet collection
jetShort = {'Topo':'To',
            'PFlow':'Pf'}


def submitJob(fname,job_options,cmd,fout=None):
    '''
    Write a submission script and submit the job

    Inputs:
    - fname: The name for the file
    - job_options: Dictionary for the LSF batch job options
    - cmd: The command that you want to submit to the batch

    '''

    f = open(fname,"w")

    # write the output
    f.write("#!/bin/bash -l\n#\n")
    for flag, val in job_options.items():
        f.write('#BSUB -{} {}\n'.format(flag,val))
    f.write(cmd+'\n')

    # Close the file
    f.close()

    # Make it executable
    ex = "chmod a+x {}".format(fname)
    os.system(ex)

    sub = "bsub < {}".format(fname)
    if fout is None:
        # Submit the job
        os.system(sub)
    else:
        fout.write(sub+"\n")

def step1():




    pass

def step2():

    vStr = "sd0_sz0_nNextToInnHits_nInnHits_nsharedBLHits_nsplitBLHits_nsharedPixHits_nsplitPixHits_nsharedSCTHits_logNorm_ptfrac_dr_norm_nPixHits_nSCTHits"

    job_options = {
        'W': '80:00',
        'q': 'atlas-t3',
        'B':''
    }

    jetCollection = 'PFlow'

    scalingfile = 'data/mc16d_PFlow_ttbar/scale_3mtrain_15trks_{}.json'.format(vStr)

    inputArgs = {
        'jetCollection':jetCollection,
        'nJets':int(1.5e6),
        'mode':'test',
    }

    fout = open("step2_tmp.sh","w")
    fout.write("#!/bin/bash -u\n\n")

    # Part 1: Submit jobs to process the test files

    # For this in initial processing of the test files, I'm just going
    # to scale w/r.t. the ttbar data, and then look at the scalings w/r.t
    # the other trainings in step 3!
    for mc in ['mc16a','mc16d']:
        for physicsSample in ['ttbar','Zprime_1.5TeV','Zprime_5TeV']:

            if mc == 'mc16a' and physicsSample == 'Zprime_5TeV':
                continue

            subDir = '{}_{}_{}'.format(mc,jetCollection,physicsSample)

            job_name = '{}_test'.format(subDir.replace(".","p"))
            job_options['J'] = job_name
            #job_options['e'] = "output/{}-%J.log".format(job_name)
            job_options['o'] = "output/{}-%J.log".format(job_name)

            # Open the input file

            inputArgs['mc'] = mc
            inputArgs['physicsSample'] = physicsSample

            # For the non-hybrid files, use even #s for the test data
            if 'hybrid' in subDir:
                inputArgs['filename'] = '../public/FrancescoNtuples/{}/{}'.format(subDir, Ntuples[subDir])
            else:
                inputArgs['filename'] = "\'../public/" + Ntuples[subDir].format('02468') + "\'"

            # Create the info for the scale tag
            if mc == 'mc16d' and physicsSample == 'ttbar':
                inputArgs['scale_tag'] = ''
                inputArgs['scalingfile'] = ''
            else:
                inputArgs['scale_tag'] = '_{}{}Scale'.format('' if mc == 'mc16d' else 'mc16d',
                                                             '' if physicsSample=='ttbar' else 'ttbar')
                inputArgs['scalingfile'] = scalingfile

            cmd = "python root_to_np.py"
            for key, val in inputArgs.items():
                if type(val) != str or len(val) > 0:
                    cmd += " --{} {}".format(key,val)

            # Create the script and submit the job
            #fname = job_name + "_tmp.job"
            #print("Submitting {}".format(job_name))
            #submitJob(fname,job_options,cmd,fout)

            fout.write('bsub -W 80:00 -q atlas-t3 -oo "output/' + job_name + '.txt" -J "' + job_name + '" -B "' + cmd + '"\n')

    fout.close()
    os.system("chmod a+x step2_tmp.sh")

    # Part 2: Locally process the training files, i.e, the ttbar with the grade
    #inputArgs['mode'] = 'train'

    #cmd = 'python root_to_np.py --mc mc16d --jetCollection PFlow --physicsSample ttbar'
    #cmd += ' --filename data/mc16d_PFlow_ttbar/jet_3mtrain_15trks_sd0_rev.h5 --nJets 3000000 --mode train'
    #cmd += ' --noNormVars sd0,sz0,ptfrac,dr,ip3d_grade --logNormVars -1 --jointNormVars -1'

    #print('Get the ttbar with grade training file')
    #os.system(cmd)

def step3():
    '''
    Process some more test files by just rescaling the prev studies
    '''

    inputArgs = {
        'mc':'mc16d',
        'jetCollection':'PFlow',
        'nJets':int(1.5e6),
        'mode':'test',
        'physicsSample':'ttbar'
    }

    print('1. ttbar mc16d w/ the grade vars')
    # which is w/o any scaling, b/c none of the vars are normalized
    inputArgs['filename'] = 'data/mc16d_PFlow_ttbar/jet_2mtest_15trks_sd0_rev.h5'

    cmd = 'python root_to_np.py  --noNormVars sd0,sz0,ptfrac,dr,ip3d_grade --logNormVars -1 --jointNormVars -1'
    for key, val in inputArgs.items():
        cmd += " --{} {}".format(key,val)

    # For now, I'm just testing that I like the command structure
    print('\n'+cmd+'\n')
    os.system(cmd)


    '''
    Hybrid scaling for ttbar, Zprime (for mc16a and d)
    '''
    print('\n2. Hybrid scalings')

    vStr = 'sd0_sz0_nNextToInnHits_nInnHits_nsharedBLHits_nsplitBLHits_nsharedPixHits_nsplitPixHits_nsharedSCTHits_logNorm_ptfrac_dr_norm_nPixHits_nSCTHits'
    inputArgs['scalingfile'] = 'data/mc16d_PFlow_hybrid_1.5TeV/scale_3mtrain_15trks_{}.json'.format(vStr)
    inputArgs['scale_tag'] = '_hybrid_1.5TeV_scale'

    for mc in ['mc16a','mc16d']:
        for physicsSample in ['ttbar','Zprime_1.5TeV']:

            inputArgs['mc'] = mc
            inputArgs['physicsSample'] = physicsSample
            inputArgs['filename'] = 'data/{}_PFlow_{}/jet_2mtest_15trks_sd0_rev.h5'.format(mc,physicsSample)

            cmd = 'python root_to_np.py'
            for key, val in inputArgs.items():
                cmd += " --{} {}".format(key,val)

            print('\n'+cmd+'\n')
            os.system(cmd)


    '''
    Extended hybrid scaling for ttbar, Zprime, and extended Zprime (just mc16d for now)
    '''
    print('\n3. Extended hybrid scalings')

    inputArgs['scalingfile'] = 'data/mc16d_PFlow_hybrid_5TeV/scale_3mtrain_15trks_{}.json'.format(vStr)
    inputArgs['scale_tag'] = '_hybrid_5TeV_scale'

    for physicsSample in ['ttbar','Zprime_1.5TeV','Zprime_5TeV']:

        inputArgs['physicsSample'] = physicsSample
        inputArgs['filename'] = 'data/{}_PFlow_{}/jet_2mtest_15trks_sd0_rev.h5'.format(mc,physicsSample)

        cmd = 'python root_to_np.py'
        for key, val in inputArgs.items():
            cmd += " --{} {}".format(key,val)

        print('\n'+cmd+'\n')
        os.system(cmd)




if __name__ == '__main__':

    from argparse import ArgumentParser

    # Start off just using the same OptionParser arguments that Zihao used.
    p = ArgumentParser()
    p.add_argument('--step',type=int, default='3', dest="step",
                   help="Which step of the preprocessing we're on.")
    # I could also add some args like Topo or Plow, which mc campaigns to process, etc.

    args = p.parse_args()

    if args.step == 1:
        print('Error: Do not yet have functionality for step {}'.format(args.step))
    elif args.step == 2:
        step2()
    elif args.step == 3:
        step3()
    else:
        print('Error: Do not yet have functionality for step {}'.format(args.step))
